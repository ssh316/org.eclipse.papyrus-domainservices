/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.edges.IDomainBasedEdgeContainerProvider;
import org.eclipse.papyrus.uml.domain.services.edges.IDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.modify.IFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.UMLFactory;

/**
 * Object in charge of creation domain edge base.
 *
 * @author Arthur Daussy
 */
public class ElementBasedEdgeCreator implements IDomainBasedEdgeCreator {

    private final IDomainBasedEdgeContainerProvider containerProvider;

    private final IDomainBasedEdgeInitializer edgeInitializer;

    private final IElementConfigurer elementInitializer;

    private final IFeatureModifier featureModifier;

    public ElementBasedEdgeCreator(IDomainBasedEdgeContainerProvider containerProvider,
            IDomainBasedEdgeInitializer edgeInitializer, IElementConfigurer elementInitializer,
            IFeatureModifier featureModifier) {
        super();
        this.containerProvider = containerProvider;
        this.edgeInitializer = edgeInitializer;
        this.elementInitializer = elementInitializer;
        this.featureModifier = featureModifier;
    }

    /**
     * Creates the semantic element for a domain based edge.
     *
     * @param semanticEdgeSource
     *                           the source of the edge
     * @param semanticEdgeTarget
     *                           the target of the edge
     * @param type
     *                           the semantic type
     * @param referenceName
     *                           the name of the containment reference
     * @return the new element or <code>null</code> if unable to create a new
     *         instance
     */
    @Override
    public CreationStatus createDomainBasedEdge(EObject semanticEdgeSource, EObject semanticEdgeTarget, String type,
            String referenceName, IViewQuerier representionQuery, Object sourceView, Object targetView) {
        EClassifier eClassifier = UMLHelper.toEClass(type);
        final CreationStatus result;
        if (eClassifier instanceof EClass) {
            EClass eClass = (EClass) eClassifier;
            EObject newInstance = UMLFactory.eINSTANCE.create(eClass);

            if (newInstance != null) {
                EObject container = this.containerProvider.getContainer(semanticEdgeSource, semanticEdgeTarget,
                        newInstance, representionQuery, sourceView, targetView);

                if (container != null) {
                    result = addToContainer(container, referenceName, newInstance, semanticEdgeSource,
                            semanticEdgeTarget, representionQuery, sourceView, targetView);
                } else {
                    result = CreationStatus.createFailingStatus(
                            MessageFormat.format("Unable to find a proper container for a new {0}", type)); //$NON-NLS-1$
                }
            } else {
                result = CreationStatus
                        .createFailingStatus(MessageFormat.format("Unable to create a UML element of type {0}", type)); //$NON-NLS-1$
            }

        } else {
            result = CreationStatus.createFailingStatus(MessageFormat.format("Unkonw type {0}", type)); //$NON-NLS-1$
        }
        return result;
    }

    // CHECKSTYLE:OFF I don't know how to do better. If you do please tell me
    /**
     * Adds a newly created object into its container
     * 
     * @param container
     *                           the future container
     * @param referenceName
     *                           the name of containment reference
     * @param newInstance
     *                           the newly created instance
     * @param semanticEdgeSource
     *                           the semantic edge source
     * @param semanticEdgeTarget
     *                           the semantic edge target
     * @param representionQuery
     *                           the query interface
     * @param sourceView
     *                           the visual object at the source of the edge
     * @param targetView
     *                           the visual object at the target of the edge
     * @return a {@link CreationStatus}
     */
    private CreationStatus addToContainer(EObject container, String referenceName, EObject newInstance,
            EObject semanticEdgeSource, EObject semanticEdgeTarget, IViewQuerier representionQuery, Object sourceView,
            Object targetView) {
        Status status = featureModifier.addValue(container, referenceName, newInstance);

        final CreationStatus result;
        State state = status.getState();
        switch (state) {
        case DONE:
            // Edge specific initialization
            this.edgeInitializer.initialize(newInstance, semanticEdgeSource, semanticEdgeTarget, representionQuery,
                    sourceView, targetView);
            // Element specific initialization
            this.elementInitializer.configure(newInstance, container);

            result = CreationStatus.createOKStatus(newInstance);
            break;
        case FAILED:
            result = CreationStatus.createFailingStatus(MessageFormat
                    .format("Unable to add the new element in {0}using refence {1}", container, referenceName)); //$NON-NLS-1$
            break;
        case NOTHING:
            result = CreationStatus.createOKStatus(null);
            break;
        default:
            throw new IllegalStateException("Unkown state " + state);
        }
        return result;
    }
    // CHECKSTYLE:ON

}
