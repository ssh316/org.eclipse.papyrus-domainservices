/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;

/**
 * Object in charge of checking if a target reconnection is possible for a
 * domain based edge.
 * 
 * @author Jessy MALLET
 *
 */
public interface IDomainBasedEdgeReconnectionTargetChecker {
    /**
     * Check reconnection of the semantic element target for a domain based edge.
     *
     * @param edgeToReconnect
     *                              the semantic edge to reconnect
     * @param oldSemanticEdgeTarget
     *                              the old target of the edge
     * @param newSemanticEdgeTarget
     *                              the new target of the edge
     * @return a {@link CheckStatus}
     */
    CheckStatus canReconnect(EObject edgeToReconnect, EObject oldSemanticEdgeTarget, EObject newSemanticEdgeTarget);

}
