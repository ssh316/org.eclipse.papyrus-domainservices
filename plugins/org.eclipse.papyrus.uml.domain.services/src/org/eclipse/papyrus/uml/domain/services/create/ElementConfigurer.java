/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.labels.ElementDefaultNameProvider;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Generic implementation of {@link IElementConfigurer} to be used for most UML
 * element.
 *
 * @author Arthur Daussy
 */
public class ElementConfigurer implements IElementConfigurer {

    /**
     * Initialize a semantic element.
     *
     * @param toInit
     *               the element to init
     * @param parent
     *               the context parent
     * @return the given element for fluent API use
     */
    @Override
    public EObject configure(EObject toInit, EObject parent) {
        if (toInit == null) {
            return null;
        }
        new ElementInitializerImpl(parent).doSwitch(toInit);
        return toInit;
    }

    static class ElementInitializerImpl extends UMLSwitch<Void> {
        private final EObject parent;

        ElementInitializerImpl(EObject parent) {
            super();
            this.parent = parent;
        }

        @Override
        public Void caseConstraint(Constraint constraint) {
            OpaqueExpression spec = UMLFactory.eINSTANCE.createOpaqueExpression();
            spec.setName("constraintSpec"); //$NON-NLS-1$
            spec.getLanguages().add("OCL"); //$NON-NLS-1$
            spec.getBodies().add("true"); //$NON-NLS-1$
            constraint.setSpecification(spec);
            return super.caseConstraint(constraint);
        }

        @Override
        public Void casePort(Port port) {
            port.setAggregation(AggregationKind.COMPOSITE_LITERAL);
            return super.casePort(port);
        }

        @Override
        public Void caseUseCase(UseCase useCase) {
            Element owner = useCase.getOwner();
            if (owner instanceof Classifier) {
                ((Classifier) owner).getUseCases().add(useCase);
            }
            return super.caseUseCase(useCase);
        }
        @Override
        public Void caseProperty(Property property) {
            Element owner = property.getOwner();
            if (owner instanceof Collaboration) {
                ((Collaboration) owner).getCollaborationRoles().add(property);
            }
            return super.caseProperty(property);
        }

        @Override
        public Void caseNamedElement(NamedElement namedElement) {
            namedElement.setName(new ElementDefaultNameProvider().getDefaultName(namedElement, this.parent));
            return super.caseNamedElement(namedElement);
        }
    }
}
