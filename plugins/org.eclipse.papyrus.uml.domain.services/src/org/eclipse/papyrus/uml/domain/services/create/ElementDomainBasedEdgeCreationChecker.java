/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.create;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;

public class ElementDomainBasedEdgeCreationChecker implements IDomainBasedEdgeCreationChecker {

    @Override
    public CheckStatus canCreate(EObject semanticEdgeSource, EObject semanticEdgeTarget, String type,
            String referenceName, IViewQuerier representationQuery, Object sourceView, Object targetView) {
        final CheckStatus result;
        switch (UMLHelper.toEClass(type).getClassifierID()) {
        case UMLPackage.ASSOCIATION:
            result = handleAssociation(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.CONNECTOR:
            result = handleConnector(representationQuery, sourceView, targetView);
            break;
        case UMLPackage.USAGE:
            result = handleUsage(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.GENERALIZATION:
            result = handleGeneralization(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.INFORMATION_FLOW:
            result = handleInformationFlow(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.SUBSTITUTION:
            result = handleSubstitution(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.MANIFESTATION:
            result = handleManifestation(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.PACKAGE_MERGE:
            result = handlePackageMerge(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.PACKAGE_IMPORT:
            result = handlePackageImport(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.TRANSITION:
            result = handleTransition(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.INCLUDE:
            result = handleInclude(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.DEPENDENCY:
            result = handleDependency(semanticEdgeSource, semanticEdgeTarget);
            break;
        case UMLPackage.EXTEND:
            result = handleExtend(semanticEdgeSource, semanticEdgeTarget);
            break;

        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    private CheckStatus handlePackageMerge(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Package) || !(semanticEdgeTarget instanceof Package)) {
            result = CheckStatus.no("PackageImport can only be connected to Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handlePackageImport(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Namespace)) {
            result = CheckStatus.no("PackageImport can only be connected from a Namespace.");
        } else if (!(semanticEdgeTarget instanceof Package)) {
            result = CheckStatus.no("PackageImport can only be connected to Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleGeneralization(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Generalization can only be connected to Classifiers.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Generalization cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleAssociation(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Association can only be connected to Classifiers.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleSubstitution(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof Classifier) || !(semanticEdgeTarget instanceof Classifier)) {
            result = CheckStatus.no("Substitution can only be connected to Classifiers.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Substitution cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleManifestation(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof PackageableElement)) {
            result = CheckStatus.no("Manifestation can only be connected between NamedElement and PackageableElement.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Manifestation cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleInformationFlow(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            return CheckStatus.no("InformationFlow can only be connected to NamedElements.");
        }
        return CheckStatus.YES;
    }

    private CheckStatus handleDependency(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            result = CheckStatus.no("Dependency can only be connected to NamedElement.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleInclude(EObject semanticEdgeSource, EObject semanticEdgeTarget) {

        final CheckStatus result;
        if (!(semanticEdgeSource instanceof UseCase) || !(semanticEdgeTarget instanceof UseCase)) {
            result = CheckStatus.no("Include can only be connected between Use Cases.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Include cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleUsage(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof NamedElement) || !(semanticEdgeTarget instanceof NamedElement)) {
            return CheckStatus.no("Usage can only be connected to NamedElements.");
        }
        return CheckStatus.YES;
    }

    private CheckStatus handleConnector(IViewQuerier representationQuery, Object sourceView, Object targetView) {
        final CheckStatus result;
        if (sourceView == targetView) {
            result = CheckStatus.no("Cannot connect a port to itself.");
        } else {
            if (representationQuery.getBorderedNodes(sourceView).contains(targetView)
                    || (representationQuery.getBorderedNodes(targetView).contains(sourceView))) {
                result = CheckStatus.no(
                        "Cannot create a connector from a view representing a Part to its own Port (or the opposite).");
            } else if (getStructureContainers(sourceView, representationQuery).contains(targetView)
                    || getStructureContainers(targetView, representationQuery).contains(sourceView)) {
                result = CheckStatus.no(
                        "Cannot connect a Part to one of its (possibly indirect) containment, must connect to one of its Port.");
            } else {
                result = CheckStatus.YES;
            }
        }
        return result;
    }

    private CheckStatus handleTransition(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        if (!(semanticEdgeSource instanceof Vertex) || !(semanticEdgeTarget instanceof Vertex)) {
            return CheckStatus.no("A Transition can only be connected to Vertex.");
        }
        return CheckStatus.YES;
    }

    private CheckStatus handleExtend(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        final CheckStatus result;
        if (!(semanticEdgeSource instanceof UseCase) || !(semanticEdgeTarget instanceof UseCase)) {
            result = CheckStatus.no("Extend can only be connected between Use Cases.");
        } else if (semanticEdgeSource == semanticEdgeTarget) {
            result = CheckStatus.no("Extend cannot use the same element for source and target.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    /**
     * Collect container hierarchy for a view (including itself and keeping
     * containment order).
     * 
     * @param view
     *             the graphical view
     * @return the list of containing Views.
     */
    private List<Object> getStructureContainers(Object view, IViewQuerier representationQuery) {
        return representationQuery.getVisualAncestorNodes(view).stream()//
                .filter(v -> getStructuredClassifier(v, representationQuery) != null)//
                .collect(toList());

    }

    /**
     * Get the {@link StructuredClassifier} related to a View. If the view relates
     * to a Property, returns its type in case it is a StructuredClassifier.
     * 
     * @param view
     *             the graphical view
     * @return the related {@link StructuredClassifier}
     */
    private StructuredClassifier getStructuredClassifier(Object view, IViewQuerier querier) {
        StructuredClassifier structuredClassifier = null;
        EObject semanticElement = querier.getSemanticElement(view);
        if (semanticElement instanceof StructuredClassifier) {
            structuredClassifier = (StructuredClassifier) semanticElement;

        } else if (semanticElement instanceof Property) {
            Property property = (Property) semanticElement;
            if (property.getType() instanceof StructuredClassifier) {
                structuredClassifier = (StructuredClassifier) property.getType();
            }

        }
        return structuredClassifier;
    }

}
