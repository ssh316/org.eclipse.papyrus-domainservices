/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeContainerProvider;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Reconnect source behavior provider of a semantic element.
 * 
 * @author Jessy MALLET
 *
 */
public class ElementDomainBasedEdgeReconnectSourceBehaviorProvider
        implements IDomainBasedEdgeReconnectSourceBehaviorProvider {

    private final IEditableChecker editableChecker;

    public ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker editableChecker) {
        super();
        this.editableChecker = editableChecker;
    }

    @Override
    public CheckStatus reconnectSource(EObject elementToReconnect, EObject oldSource, EObject newSource) {
        if (elementToReconnect == null || oldSource == null || newSource == null) {
            return CheckStatus.no(
                    MessageFormat.format("Invalid input for reconnexion (element ={0} oldSource ={1} newSource = {2})", //$NON-NLS-1$
                            elementToReconnect, oldSource, newSource));
        }
        return new ReconnectSourceBehaviorProviderSwitch(oldSource, newSource, editableChecker)
                .doSwitch(elementToReconnect);
    }

    static class ReconnectSourceBehaviorProviderSwitch extends UMLSwitch<CheckStatus> {

        private final EObject oldSource;

        private final EObject newSource;

        private final IEditableChecker editableChecker;

        ReconnectSourceBehaviorProviderSwitch(EObject oldSource, EObject newSource, IEditableChecker editableChecker) {
            super();
            this.oldSource = oldSource;
            this.newSource = newSource;
            this.editableChecker = editableChecker;
        }

        @Override
        public CheckStatus casePackageMerge(PackageMerge object) {
            object.setReceivingPackage((Package) newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus casePackageImport(PackageImport packImport) {
            packImport.setImportingNamespace((Namespace) newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseDependency(Dependency dependency) {

            // 1. change the client of the dependency
            dependency.getClients().remove(oldSource);
            // Cast check done in Checker
            dependency.getClients().add((NamedElement) newSource);

            // 2. the dependency is owned by the nearest package of the source
            ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                    editableChecker);
            EObject newOwner = containerProvider.getContainer(newSource, dependency.getSuppliers().get(0), dependency,
                    null, null, null);
            Element oldOwner = dependency.getOwner();
            if (oldOwner != newOwner && oldOwner instanceof Package && newOwner instanceof Package) {
                ((Package) newOwner).getPackagedElements().add(dependency);
            }

            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            transition.setSource((Vertex) newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseGeneralization(Generalization generalization) {
            generalization.setSpecific((Classifier) newSource);
            return CheckStatus.YES;
        }

        public CheckStatus caseAssociation(Association association) {
            Property targetprop = association.getMemberEnds().get(1);
            targetprop.setType((Classifier) newSource);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseInclude(Include include) {
            include.setIncludingCase((UseCase) newSource);
            return CheckStatus.YES;
        }

    }

}
