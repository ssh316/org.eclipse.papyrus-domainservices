/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.services;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.uml2.uml.DurationObservation;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * This helper provides interesting features for DurationObservation objects.
 * Copy from
 * {@link org.eclipse.papyrus.uml.diagram.common.helper.DurationObservationHelper}
 * 
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class DurationObservationHelper {

    /**
     * Get the list of all DurationObservation observing duration from or to an
     * element.
     *
     * @param element
     *                              the observed element
     * @param crossReferenceAdapter
     *                              an adapter used to get inverse references
     * 
     * @return list of DurationObservation
     */
    public static List<DurationObservation> getDurationObservationsOn(NamedElement element,
            ECrossReferenceAdapter crossReferenceAdapter) {
        Collection<Setting> inverseReferences = crossReferenceAdapter.getInverseReferences(element, false);
        // DurationObservation referencing element
        List<DurationObservation> referencing1 = new LinkedList<DurationObservation>();
        for (Setting ref : inverseReferences) {
            if (UMLPackage.eINSTANCE.getDurationObservation_Event().equals(ref.getEStructuralFeature())
                    && ref.getEObject().eContainer() != null) {
                referencing1.add((DurationObservation) ref.getEObject());
            }
        }
        return referencing1;
    }
}
