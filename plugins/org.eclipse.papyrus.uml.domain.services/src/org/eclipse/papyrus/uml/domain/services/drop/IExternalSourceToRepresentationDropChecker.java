/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;

/**
 * Object in charge of checking if a semantic D&D is possible.
 * 
 * @author Jessy MALLET
 *
 */
public interface IExternalSourceToRepresentationDropChecker {

    /**
     * Check drag and drop of the semantic element.
     *
     * @param elementToDrop
     *                             the semantic element to drop
     * @param newSemanticContainer
     *                             the new container of the semantic element
     * @return a {@link CanCreateStatus}
     */
    CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer);

}
