/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class ElementDomainBasedEdgeReconnectionSourceChecker implements IDomainBasedEdgeReconnectionSourceChecker {

    private final IEditableChecker editableCheck;

    public ElementDomainBasedEdgeReconnectionSourceChecker(IEditableChecker editableCheck) {
        super();
        this.editableCheck = editableCheck;
    }

    @Override
    public CheckStatus canReconnect(EObject edgeToReconnect, EObject oldSemanticEdgeSource,
            EObject newSemanticEdgeSource) {
        if (newSemanticEdgeSource == null) {
            return CheckStatus.no("The new semantic edge source must not be null");
        }
        return new ElementDomainBasedEdgeReconnectionSourceCheckerSwitch(oldSemanticEdgeSource, newSemanticEdgeSource,
                editableCheck).doSwitch(edgeToReconnect);
    }

    public static class ElementDomainBasedEdgeReconnectionSourceCheckerSwitch extends UMLSwitch<CheckStatus> {

        private static final String CANNOT_EDIT_NEW_SOURCE = "Can't edit new source.";

        private final EObject oldSemanticEdgeSource;

        private final EObject newSemanticEdgeSource;

        private final IEditableChecker editableChecker;

        public ElementDomainBasedEdgeReconnectionSourceCheckerSwitch(EObject oldSemanticEdgeSource,
                EObject newSemanticEdgeSource, IEditableChecker editableChecker) {
            super();
            this.oldSemanticEdgeSource = oldSemanticEdgeSource;
            this.newSemanticEdgeSource = newSemanticEdgeSource;
            this.editableChecker = editableChecker;
        }

        @Override
        public CheckStatus caseAssociation(Association association) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof Classifier && newSemanticEdgeSource instanceof Classifier)) {
                result = CheckStatus.no("Invalid Association source");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseDependency(Dependency object) {
            final CheckStatus result;
            if (newSemanticEdgeSource instanceof NamedElement && editableChecker
                    .canEdit(newSemanticEdgeSource) /* The will be the new container of the dependency */) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus
                        .no("Dependency source can only be reconnected to a non null editable NamedElement.");
            }
            return result;
        }

        @Override
        public CheckStatus casePackageMerge(PackageMerge packMerge) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof Package && newSemanticEdgeSource instanceof Package)) {
                result = CheckStatus.no("Invalid PackageMerge source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (!(packMerge.eContainer() instanceof Package)) {
                result = CheckStatus.no("Invalid Package container");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackageImport(PackageImport packImport) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof Namespace && newSemanticEdgeSource instanceof Namespace)) {
                result = CheckStatus.no("Invalid Namespace source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (!(packImport.eContainer() instanceof Package)) {
                result = CheckStatus.no("Invalid Package container");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            final CheckStatus result;
            if (!editableChecker.canEdit(transition)) {
                result = CheckStatus.no("Can't edit the Transition.");
            } else if (!(oldSemanticEdgeSource instanceof Vertex && newSemanticEdgeSource instanceof Vertex)) {
                result = CheckStatus.no("Invalid semantic source");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseGeneralization(Generalization generalization) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof Classifier && newSemanticEdgeSource instanceof Classifier)) {
                result = CheckStatus.no("Invalid Classifier source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                // the new semantic source contains the generalization
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (generalization.getGeneral() == newSemanticEdgeSource) {
                result = CheckStatus.no("Generalization cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseSubstitution(Substitution substitution) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof Classifier && newSemanticEdgeSource instanceof Classifier)) {
                result = CheckStatus.no("Invalid Classifier source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                // the new semantic source contains the substitution
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (substitution.getContract() == newSemanticEdgeSource) {
                result = CheckStatus.no("Substitution cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseUsage(Usage usage) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof NamedElement && newSemanticEdgeSource instanceof NamedElement)) {
                result = CheckStatus.no("Invalid NamedElement source");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInclude(Include include) {
            final CheckStatus result;
            if (!(oldSemanticEdgeSource instanceof UseCase && newSemanticEdgeSource instanceof UseCase)) {
                result = CheckStatus.no("Invalid Use Case source");
            } else if (!editableChecker.canEdit(newSemanticEdgeSource)) {
                result = CheckStatus.no(CANNOT_EDIT_NEW_SOURCE);
            } else if (include.getAddition() == newSemanticEdgeSource) {
                result = CheckStatus.no("Include cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.YES;
        }
    }

}
