/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;

/**
 * Provider of behavior when reconnect source of semantic elements.
 * 
 * @author Jessy MALLET
 *
 */
public interface IDomainBasedEdgeReconnectSourceBehaviorProvider {

    /**
     * Reconnect source of a semantic element to an other one.
     * 
     * @param elementToReconnect
     *                           the element being reconnected
     * @param oldSource
     *                           the semantic element pointed by the edge as Source
     *                           before reconnecting
     * @param newSource
     *                           the semantic element pointed by the edge as Source
     *                           after reconnecting
     * @return a Status of the reconnection
     */
    CheckStatus reconnectSource(EObject elementToReconnect, EObject oldSource, EObject newSource);

}
