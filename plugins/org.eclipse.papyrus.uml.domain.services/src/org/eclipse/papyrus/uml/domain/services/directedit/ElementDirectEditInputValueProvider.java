/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.directedit;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Service in charge of providing the default value when the user start the
 * execution of a direct edit tool.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDirectEditInputValueProvider implements IDirectEditInputValueProvider {

    private static final String EMPTY = "";

    @Override
    public String getDirectEditInputValue(EObject eObject) {
        if (eObject != null) {
            return new ElementDirectEditInputValueProviderSwitch().doSwitch(eObject);
        } else {
            return EMPTY;
        }
    }

    public static class ElementDirectEditInputValueProviderSwitch extends UMLSwitch<String> {

        @Override
        public String caseNamedElement(NamedElement namedElement) {
            return namedElement.getName();
        }

        @Override
        public String caseComment(Comment comment) {
            return comment.getBody();
        }

        @Override
        public String caseTransition(Transition transition) {
            EList<Comment> ownedComments = transition.getOwnedComments();
            if (!ownedComments.isEmpty()) {
                return ownedComments.get(0).getBody();
            }
            return "";
        }

        @Override
        public String defaultCase(EObject object) {
            return EMPTY;
        }
    }

}
