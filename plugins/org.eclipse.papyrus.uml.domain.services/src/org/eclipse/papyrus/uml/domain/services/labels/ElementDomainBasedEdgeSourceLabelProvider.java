/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.labels.domains.DefaultNamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.INamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.MultiplicityLabelHelper;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Provider of a label for domain based edge on the source (label located at the
 * beginning of an edge).
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDomainBasedEdgeSourceLabelProvider implements IDomainBasedEdgeEndLabelProvider {

    private INamedElementNameProvider namedElementNameProvider;

    public ElementDomainBasedEdgeSourceLabelProvider(INamedElementNameProvider namedElementNameProvider) {
        this.namedElementNameProvider = namedElementNameProvider;
    }

    public static ElementDomainBasedEdgeSourceLabelProvider buildDefault() {
        return new ElementDomainBasedEdgeSourceLabelProvider(new DefaultNamedElementNameProvider());
    }

    @Override
    public String getLabel(EObject element, EObject semanticEnd) {
        String label = new ElementDomainBasedEdgeSourceLabelProviderSwitch(namedElementNameProvider).doSwitch(element);

        if (label == null) {
            label = "";
        }
        return label;
    }

    public static final class ElementDomainBasedEdgeSourceLabelProviderSwitch extends UMLSwitch<String> {

        private MultiplicityLabelHelper multiplicityLabelHelper;

        public ElementDomainBasedEdgeSourceLabelProviderSwitch(INamedElementNameProvider namedElementNameProvider) {
            super();
            this.multiplicityLabelHelper = new MultiplicityLabelHelper(namedElementNameProvider);
        }

        @Override
        public String caseConnector(Connector connector) {

            EList<ConnectorEnd> ends = connector.getEnds();
            if (ends.size() > 0) {
                return multiplicityLabelHelper.formatMultiplicityNoBrackets(ends.get(0), false);
            }
            return super.caseConnector(connector);
        }

    }
}
