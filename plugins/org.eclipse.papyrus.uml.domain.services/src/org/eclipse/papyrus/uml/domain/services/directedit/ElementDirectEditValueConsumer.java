/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.directedit;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Most generic implementation of the {@link IDirectEditValueConsumer} for UML
 * elements.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDirectEditValueConsumer implements IDirectEditValueConsumer {

    @Override
    public CheckStatus consumeNewLabel(EObject owner, String editedText) {

        if (owner != null) {
            return new ElementDirectEditValueConsumerSwitch(editedText).doSwitch(owner);
        }
        return CheckStatus.no("Can't execute a Direct Edit tool on null");
    }

    public static class ElementDirectEditValueConsumerSwitch extends UMLSwitch<CheckStatus> {

        private final String editedText;

        public ElementDirectEditValueConsumerSwitch(String editedText) {
            super();
            this.editedText = editedText;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            comment.setBody(editedText);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseNamedElement(NamedElement nameElement) {
            nameElement.setName(editedText);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            Comment comment = transition.getOwnedComments().stream().findFirst().orElseGet(() -> {
                Comment createdComment = UMLFactory.eINSTANCE.createComment();
                transition.getOwnedComments().add(createdComment);
                return createdComment;
            });
            comment.setBody(editedText);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("[Direct Edit] Unhandled element : " + object.eClass().getName());
        }

    }

}
