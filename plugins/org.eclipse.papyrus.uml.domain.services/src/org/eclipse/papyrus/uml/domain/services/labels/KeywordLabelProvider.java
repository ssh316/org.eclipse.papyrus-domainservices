/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import java.util.function.Function;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.ProtocolStateMachine;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.VisibilityKind;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Computes the prefix representing all keywords applied to a given element.
 * 
 * @author Jessy MALLET
 *
 */
public class KeywordLabelProvider implements Function<EObject, String> {

    @Override
    public String apply(EObject t) {
        if (t != null) {
            String keyword = new KeywordProviderSwitch().doSwitch(t);
            if (keyword != null) {
                return UMLCharacters.ST_LEFT + keyword + UMLCharacters.ST_RIGHT;
            }
        }
        return null;
    }

    static class KeywordProviderSwitch extends UMLSwitch<String> {

        @Override
        public String caseAbstraction(Abstraction object) {
            if (!(object instanceof Realization)) {
                return "abstraction";
            }
            return super.caseAbstraction(object);
        }

        @Override
        public String caseActivity(Activity object) {
            return "activity";
        }

        @Override
        public String caseCollaboration(Collaboration object) {
            return "collaboration";
        }

        @Override
        public String caseComponent(Component object) {
            return "component";
        }

        @Override
        public String caseExtend(Extend extend) {
            return "extend";
        }

        @Override
        public String caseFunctionBehavior(FunctionBehavior functionBehavior) {
            return "functionBehavior";
        }

        @Override
        public String caseInclude(Include object) {
            return "include";
        }

        @Override
        public String caseInformationFlow(InformationFlow flow) {
            return "flow";
        }

        @Override
        public String caseInformationItem(InformationItem object) {
            return "information";
        }

        @Override
        public String caseInteraction(Interaction interaction) {
            return "interaction";
        }

        @Override
        public String caseInterface(Interface object) {
            return "interface";
        }

        @Override
        public String caseManifestation(Manifestation object) {
            return "manifest";
        }

        @Override
        public String caseOpaqueBehavior(OpaqueBehavior opaqueBehavior) {
            return "opaqueBehavior";
        }

        @Override
        public String casePackageImport(PackageImport object) {
            if (object.getVisibility() == VisibilityKind.PUBLIC_LITERAL) {
                return "import";
            } else {
                return "access";
            }
        }

        @Override
        public String casePackageMerge(PackageMerge object) {
            return "merge";
        }

        @Override
        public String caseProtocolStateMachine(ProtocolStateMachine protocolStateMachine) {
            return "protocol";
        }

        @Override
        public String caseSignal(Signal signal) {
            return "signal";
        }

        @Override
        public String caseStateMachine(StateMachine stateMachine) {
            return "stateMachine";
        }

        @Override
        public String caseSubstitution(Substitution object) {
            return "substitute";
        }

        @Override
        public String caseUsage(Usage object) {
            return "use";
        }

    }

}
