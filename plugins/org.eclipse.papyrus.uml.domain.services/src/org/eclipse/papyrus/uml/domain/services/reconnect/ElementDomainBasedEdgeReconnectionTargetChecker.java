/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class ElementDomainBasedEdgeReconnectionTargetChecker implements IDomainBasedEdgeReconnectionTargetChecker {

    private final IEditableChecker editableChecker;

    public ElementDomainBasedEdgeReconnectionTargetChecker(IEditableChecker editableChecker) {
        super();
        this.editableChecker = editableChecker;
    }

    @Override
    public CheckStatus canReconnect(EObject edgeToReconnect, EObject oldSemanticEdgeTarget,
            EObject newSemanticEdgeTarget) {
        if (newSemanticEdgeTarget == null) {
            return CheckStatus.no("The new semantic edge target must not be null");
        }
        return new ElementDomainBasedEdgeReconnectionTargetCheckerSwitch(oldSemanticEdgeTarget, newSemanticEdgeTarget,
                editableChecker).doSwitch(edgeToReconnect);
    }

    public static class ElementDomainBasedEdgeReconnectionTargetCheckerSwitch extends UMLSwitch<CheckStatus> {

        private final EObject oldSemanticEdgeTarget;

        private final EObject newSemanticEdgeTarget;
        private final IEditableChecker editableChecker;

        public ElementDomainBasedEdgeReconnectionTargetCheckerSwitch(EObject oldSemanticEdgeTarget,
                EObject newSemanticEdgeTarget, IEditableChecker editableChecker) {
            super();
            this.oldSemanticEdgeTarget = oldSemanticEdgeTarget;
            this.newSemanticEdgeTarget = newSemanticEdgeTarget;
            this.editableChecker = editableChecker;
        }

        @Override
        public CheckStatus caseAssociation(Association association) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Classifier && newSemanticEdgeTarget instanceof Classifier)) {
                result = CheckStatus.no("Invalid Association target");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseDependency(Dependency object) {
            final CheckStatus result;
            if (newSemanticEdgeTarget == null || !(newSemanticEdgeTarget instanceof NamedElement)) {
                result = CheckStatus.no("Dependency target can only be reconnected to a non null NamedElement.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackageMerge(PackageMerge object) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Package && newSemanticEdgeTarget instanceof Package)) {
                result = CheckStatus.no("Invalid PackageMerge source or target");
            } else if (!(object.eContainer() instanceof Package)) {
                result = CheckStatus.no("Invalid Package container");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus casePackageImport(PackageImport object) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Package && newSemanticEdgeTarget instanceof Package)) {
                result = CheckStatus.no("Invalid PackageImport source or target");
            } else if (!(object.eContainer() instanceof Package)) {
                result = CheckStatus.no("Invalid Package container");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            final CheckStatus result;
            if (!editableChecker.canEdit(transition)) {
                result = CheckStatus.no("Can't edit the Transition.");
            } else if (!(oldSemanticEdgeTarget instanceof Vertex && newSemanticEdgeTarget instanceof Vertex)) {
                result = CheckStatus.no("Invalid semantic source");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseGeneralization(Generalization generalization) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Classifier && newSemanticEdgeTarget instanceof Classifier)) {
                result = CheckStatus.no("Invalid Classifier target");
            } else if (generalization.getSpecific() == newSemanticEdgeTarget) {
                result = CheckStatus.no("Generalization cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseSubstitution(Substitution substitution) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof Classifier && newSemanticEdgeTarget instanceof Classifier)) {
                result = CheckStatus.no("Invalid Classifier target");
            } else if (substitution.getSubstitutingClassifier() == newSemanticEdgeTarget) {
                result = CheckStatus.no("Substitution cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseUsage(Usage usage) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof NamedElement && newSemanticEdgeTarget instanceof NamedElement)) {
                result = CheckStatus.no("Invalid NamedElement target");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseInclude(Include include) {
            final CheckStatus result;
            if (!(oldSemanticEdgeTarget instanceof UseCase && newSemanticEdgeTarget instanceof UseCase)) {
                result = CheckStatus.no("Invalid Use Case target");
            } else if (include.getIncludingCase() == newSemanticEdgeTarget) {
                result = CheckStatus.no("Include cannot use the same element for source and target.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.YES;
        }
    }
}
