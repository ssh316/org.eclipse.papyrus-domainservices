/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Class in charge to check if the creation of a semanticObject is possible.
 * 
 * @author lfasani
 *
 */
public class ElementCreationChecker implements ICreatorChecker {

    @Override
    public CheckStatus canCreate(EObject parent, String type, String containmentReferenceName) {
        final CheckStatus result;
        switch (UMLHelper.toEClass(type).getClassifierID()) {
        case UMLPackage.REGION:
            result = handleRegion(parent);
            break;

        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    private CheckStatus handleRegion(EObject parent) {
        CheckStatus result = CheckStatus.YES;
        if (parent instanceof FinalState) {
            result = CheckStatus.no("A Final State can not have a region.");
        }
        return result;
    }
}
