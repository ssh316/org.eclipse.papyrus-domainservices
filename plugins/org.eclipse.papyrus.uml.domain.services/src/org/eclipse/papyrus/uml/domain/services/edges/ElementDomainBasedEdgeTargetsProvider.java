/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges;

import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class ElementDomainBasedEdgeTargetsProvider implements IDomainBasedEdgeTargetsProvider {

    @Override
    public List<? extends EObject> getTargets(EObject semanticElement) {
        return new EdgeProviderSwitch().doSwitch(semanticElement);
    }

    private static class EdgeProviderSwitch extends UMLSwitch<List<? extends EObject>> {

        private List<? extends EObject> adaptOptionalSingleton(EObject value) {
            if (value != null) {
                return Collections.singletonList(value);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseAssociation(Association object) {
            // Target of association is the type of the source property
            Type type = object.getMemberEnds().get(0).getType();
            if (type != null) {
                return Collections.singletonList(type);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseConnector(Connector connector) {
            EObject source = new ElementDomainBasedEdgeSourceProvider().getSource(connector);
            if (source == null) {
                return null;
            }

            /*
             * This semantically incorrect. A user should be able to connect a Connector
             * with a source.role and target.role which are identical (mostly is the have
             * different partWithPort value) However, we need to keep that implementation
             * until we have precondition that could add check visual representation
             * elements. https://github.com/PapyrusSirius/papyrus-web/issues/41
             */
            return connector.getEnds().stream()//
                    .filter(end -> end.getRole() != null && end.getRole() != source)//
                    .map(end -> end.getRole())//
                    .filter(role -> role != source)//
                    .collect(toList());
        }

        @Override
        public List<? extends EObject> caseGeneralization(Generalization generatization) {
            Classifier general = generatization.getGeneral();
            if (general != null) {
                return Collections.singletonList(general);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseInclude(Include include) {
            UseCase useCase = include.getAddition();
            if (useCase != null) {
                return Collections.singletonList(useCase);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseSubstitution(Substitution substitution) {
            Classifier contract = substitution.getContract();
            if (contract != null) {
                return Collections.singletonList(contract);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseManifestation(Manifestation manifestation) {
            PackageableElement utilizedElement = manifestation.getUtilizedElement();
            if (utilizedElement != null) {
                return Collections.singletonList(utilizedElement);
            } else {
                return Collections.emptyList();
            }
        }

        @Override
        public List<? extends EObject> caseDependency(Dependency object) {
            return List.copyOf(object.getSuppliers());
        }

        @Override
        public List<? extends EObject> caseInformationFlow(InformationFlow informationFlow) {
            return List.copyOf(informationFlow.getInformationTargets());
        }

        @Override
        public List<? extends EObject> casePackageMerge(PackageMerge object) {
            return adaptOptionalSingleton(object.getMergedPackage());
        }

        @Override
        public List<EObject> defaultCase(EObject object) {
            return List.of();
        }

        @Override
        public List<? extends EObject> casePackageImport(PackageImport object) {
            return adaptOptionalSingleton(object.getImportedPackage());
        }

        @Override
        public List<? extends EObject> caseTransition(Transition transition) {
            return List.of(transition.getTarget());
        }

        @Override
        public List<? extends EObject> caseExtend(Extend extend) {
            UseCase useCase = extend.getExtendedCase();
            if (useCase != null) {
                return Collections.singletonList(useCase);
            } else {
                return Collections.emptyList();
            }
        }
    }
}
