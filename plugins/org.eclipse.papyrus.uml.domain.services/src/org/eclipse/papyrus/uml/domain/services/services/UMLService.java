/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.services;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.EMFUtils;
import org.eclipse.papyrus.uml.domain.services.UMLHelper;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionOperand;

/**
 * Generic service for UML metamodel.
 *
 * @author Arthur Daussy
 */
public class UMLService {

    /**
     * Gets all reachable elements of a type in the {@link ResourceSet} of given
     * {@link EObject}.
     *
     * @param eObject
     *                the {@link EObject} stored in a {@link ResourceSet}
     * @param type
     *                the search typed (either simple or qualified named of the
     *                EClass ("Class" vs "uml::Class")
     * @return a list of reachable object
     */
    public List<EObject> getAllReachable(EObject eObject, String type) {
        return getAllReachable(eObject, type, true);
    }

    /**
     * Gets all reachable elements of a type in the {@link ResourceSet} of given
     * {@link EObject}.
     *
     * @param eObject
     *                    the {@link EObject} stored in a {@link ResourceSet}
     * @param type
     *                    the search typed (either simple or qualified named of the
     *                    EClass ("Class" vs "uml::Class")
     * @param withSubType
     *                    holds true to include any element with a compatible type,
     *                    <code>false</code> otherwise
     * @return a list of reachable object
     */
    public List<EObject> getAllReachable(EObject eObject, String type, boolean withSubType) {
        ResourceSet rs = eObject.eResource().getResourceSet();
        EClassifier eClass = UMLHelper.toEClass(type);
        if (eClass != null) {
            final Predicate<Notifier> predicate;
            if (withSubType) {
                predicate = e -> e instanceof EObject && eClass.isInstance(e);
            } else {
                predicate = e -> e instanceof EObject && eClass == ((EObject) e).eClass();
            }

            return UMLService.getUmlResources(rs.getResources())//
                    .flatMap(r -> EMFUtils.eAllContentSteamWithSelf(r))//
                    .filter(predicate)//
                    .map(EObject.class::cast)//
                    .collect(toList());
        } else {
            return Collections.emptyList();
        }
    }

    private static Stream<Resource> getUmlResources(EList<Resource> resources) {
        return resources.stream().filter(r -> !r.getContents().isEmpty() && r.getContents().get(0) instanceof Element);
    }

    /**
     * Check if a given {@link EObject} is an Interaction container.
     * 
     * @param object
     *               the Object to check
     * @return <code>true</code> if the given {@link EObject} is an Interaction
     *         container, <code>false</code> otherwise.
     */
    public static boolean isInteractionContainer(EObject object) {
        return object instanceof InteractionOperand || object instanceof CombinedFragment
                || object instanceof Interaction;
    }

    /**
     * Uses a reverse reference map that is maintained by the MSL service to find
     * all referencers of a particular element. The search can be narrowed down by
     * passing the list of Reference features to match. <code>features</code> can be
     * null.
     * 
     * @param eObject
     *                 The referenced object.
     * @param features
     *                 The reference features.
     * @return The collection of referencers.
     */
    public static Collection<EObject> getReferencers(EObject eObject, EReference[] features,
            ECrossReferenceAdapter crossReferenceAdapter) {
        ArrayList<EObject> referencers = new ArrayList<EObject>();
        if (crossReferenceAdapter != null) {
            Collection<Setting> settings = crossReferenceAdapter.getInverseReferences(eObject);

            if (!settings.isEmpty()) {
                int count;
                // CHECKSTYLE:OFF Papyrus Legacy Code
                if ((features != null) && ((count = features.length) != 0)) {
                    // CHECKSTYLE:ON Papyrus Legacy Code
                    Iterator<Setting> it = settings.iterator();
                    while (it.hasNext()) {
                        Setting setting = it.next();
                        EStructuralFeature feature = setting.getEStructuralFeature();
                        for (int i = 0; i < count; ++i) {
                            if (feature == features[i]) {
                                referencers.add(setting.getEObject());
                                break;
                            }
                        }
                    }
                } else {
                    Iterator<Setting> it = settings.iterator();
                    while (it.hasNext()) {
                        referencers.add(it.next().getEObject());
                    }
                }
            }
        }
        return referencers;
    }

    /**
     * <pre>
     * Test if the used element is referenced by other elements than the known
     * referencer (except its container). It ignores references from an other meta-model.
     * </pre>
     *
     * @param usedObject
     *                        the used object
     * @param knownReferencer
     *                        the known referencer
     * @return true if the known referencer is the only referencer.
     */
    public static boolean isOnlyUsage(EObject usedObject, EObject knownReferencer,
            ECrossReferenceAdapter crossReferenceAdapter) {
        boolean isUsed = false;
        EPackage mmPackage = usedObject.eClass().getEPackage();

        // Retrieve the list of elements referencing the usedObject.
        Set<EObject> crossReferences = new HashSet<>();
        for (Setting setting : crossReferenceAdapter.getInverseReferences(usedObject, false)) {
            EObject eObj = setting.getEObject();
            if (eObj.eClass().getEPackage().equals(mmPackage)) {
                crossReferences.add(eObj);
            }
        }

        // Remove the container of used object.
        crossReferences.remove(usedObject.eContainer());
        // Remove the knownReferencer from the list of references.
        crossReferences.remove(knownReferencer);

        // If no referencer remains in the list, the known element is the only
        // usage.
        if (crossReferences.isEmpty()) {
            isUsed = true;
        }

        return isUsed;
    }
}
