/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Implementation of {@link IDestroyerChecker} for any element.
 * 
 * @author Laurent Fasani
 */
public class ElementDestroyerChecker implements IDestroyerChecker {
    private final IEditableChecker editableChecker;

    private final ECrossReferenceAdapter crossReferenceAdapter;

    public ElementDestroyerChecker(ECrossReferenceAdapter crossReferenceAdapter, IEditableChecker editableChecker) {
        this.editableChecker = editableChecker;
        this.crossReferenceAdapter = crossReferenceAdapter;
    }

    @Override
    public DestroyerStatus canDestroy(Set<EObject> objectsToDelete) {
        DestroyerStatus destroyerStatus = DestroyerStatus.createOKStatus(objectsToDelete);
        // All element must be editable
        // All container of elements to delete must be editable
        // All elements that have a cross ref to the element to delete must be editable
        Stream<EObject> crossReferenceOwners = objectsToDelete.stream()//
                .flatMap(object -> crossReferenceAdapter.getInverseReferences(object).stream()) // also return
                                                                                                // containment settings
                .map(setting -> setting.getEObject());

        Set<EObject> notEditableObjects = Stream.concat(objectsToDelete.stream(), crossReferenceOwners)//
                .filter(object -> !editableChecker.canEdit(object))//
                .collect(Collectors.toCollection(LinkedHashSet::new));

        if (!notEditableObjects.isEmpty()) {
            destroyerStatus = DestroyerStatus.createFailingStatus(
                    "One of the elements, impacted by the elements to delete, can not be edited", notEditableObjects);
        } else {
            // Specific rule for each element
            DeletableElementCheckerSwitch destroyerCheckerSwitch = new DeletableElementCheckerSwitch(objectsToDelete);
            destroyerStatus = objectsToDelete.stream()//
                    .map(object -> destroyerCheckerSwitch.doSwitch(object))//
                    .filter(object -> State.FAILED.equals(object.getState()))//
                    .findFirst()//
                    .orElse(destroyerStatus);
        }

        return destroyerStatus;
    }

    class DeletableElementCheckerSwitch extends UMLSwitch<DestroyerStatus> {
        private Set<EObject> objectsToDelete;

        DeletableElementCheckerSwitch(Set<EObject> objectsToDelete) {
            this.objectsToDelete = objectsToDelete;
        }

        /**
         * Forbid the destroy if the Region is last one of a StateMachine.<br>
         * The last region of a State can be destroyed.
         */
        @Override
        public DestroyerStatus caseRegion(Region region) {
            DestroyerStatus destroyerStatus = DestroyerStatus.createOKStatus(Set.of(region));
            StateMachine stateMachine = region.getStateMachine();
            // Forbid only if the region is owned by a StateMachine that is not is the
            // objects to delete.
            if (stateMachine != null && !objectsToDelete.contains(stateMachine)) {
                List<Region> stateMachineRegions = region.getStateMachine().getRegions();
                if (stateMachineRegions.size() == 1 && stateMachineRegions.contains(region)) {
                    destroyerStatus = DestroyerStatus.createFailingStatus(
                            "The region can not be delete because it is the last one of the StateMachine",
                            Set.of(region));
                }
            }
            return destroyerStatus;
        }

        @Override
        public DestroyerStatus defaultCase(EObject object) {
            return DestroyerStatus.createOKStatus(Set.of(object));
        }
    }

}
