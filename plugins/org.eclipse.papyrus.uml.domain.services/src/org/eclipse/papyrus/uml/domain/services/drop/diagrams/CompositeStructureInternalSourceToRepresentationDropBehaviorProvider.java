/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.modify.IFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ProtocolStateMachine;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a diagram element to a Composite Structure Diagram
 * Element.
 * 
 * @author Jessy MALLET
 *
 */
public class CompositeStructureInternalSourceToRepresentationDropBehaviorProvider
        implements IInternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public Status drop(EObject droppedElement, EObject oldContainer, EObject newContainer,
            ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
        return new StructureCompositeDropOutsideRepresentationBehaviorProviderSwitch(oldContainer, newContainer,
                crossRef, editableChecker).doSwitch(droppedElement);
    }

    static class StructureCompositeDropOutsideRepresentationBehaviorProviderSwitch extends UMLSwitch<Status> {

        private static final String OWNED_ATTRIBUTE = UMLPackage.eINSTANCE.getStructuredClassifier_OwnedAttribute()
                .getName();

        private static final String NESTED_CLASSIFIER_REF = UMLPackage.eINSTANCE.getClass_NestedClassifier().getName();

        private static final String PACKAGED_ELEMENT_REF = UMLPackage.eINSTANCE.getPackage_PackagedElement().getName();

        private final EObject oldContainer;

        private final EObject newContainer;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        StructureCompositeDropOutsideRepresentationBehaviorProviderSwitch(EObject oldContainer, EObject newContainer,
                ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
            super();
            this.oldContainer = oldContainer;
            this.newContainer = newContainer;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        /**
         * Default Behavior : UML element can be D&D by using the same reference
         * containment.
         * 
         * @see org.eclipse.uml2.uml.util.UMLSwitch#caseElement(org.eclipse.uml2.uml.Element)
         *
         * @param droppedElement
         *                       the element to drop
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseElement(Element droppedElement) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                String refName = droppedElement.eContainmentFeature().getName();
                if (oldContainer.eClass().getEStructuralFeature(refName) != null
                        && newContainer.eClass().getEStructuralFeature(refName) != null) {
                    dropStatus = modifier.removeValue(oldContainer, refName, droppedElement);
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(newContainer, refName, droppedElement);
                    }
                    return dropStatus;
                }
            }
            return super.caseElement(droppedElement);
        }

        @Override
        public Status caseClass(org.eclipse.uml2.uml.Class droppedClass) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                String refName = droppedClass.eContainmentFeature().getName();
                dropStatus = modifier.removeValue(oldContainer, refName, droppedClass);
                if (State.DONE == dropStatus.getState()) {
                    if (newContainer instanceof org.eclipse.uml2.uml.Class) {
                        dropStatus = modifier.addValue(newContainer, NESTED_CLASSIFIER_REF, droppedClass);
                    } else if (newContainer instanceof Package) {
                        dropStatus = modifier.addValue(newContainer, PACKAGED_ELEMENT_REF, droppedClass);
                    }
                }
                return dropStatus;
            }
            return super.caseClass(droppedClass);
        }

        @Override
        public Status caseFunctionBehavior(FunctionBehavior functionBehavior) {
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                return handleBehavioredClassifier(functionBehavior, modifier);
            }
            return super.caseFunctionBehavior(functionBehavior);
        }

        @Override
        public Status caseOpaqueBehavior(OpaqueBehavior opaqueBehavior) {
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                return handleBehavioredClassifier(opaqueBehavior, modifier);
            }
            return super.caseOpaqueBehavior(opaqueBehavior);
        }

        @Override
        public Status caseInteraction(Interaction interaction) {
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                return handleBehavioredClassifier(interaction, modifier);
            }
            return super.caseInteraction(interaction);
        }

        @Override
        public Status caseParameter(Parameter parameter) {
            Status dropStatus = new Status(State.FAILED, null);
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                String refName = parameter.eContainmentFeature().getName();
                if (newContainer instanceof org.eclipse.uml2.uml.Behavior) {
                    dropStatus = modifier.addValue(newContainer,
                            UMLPackage.eINSTANCE.getBehavior_OwnedParameter().getName(), parameter);
                }
                if (State.DONE == dropStatus.getState()) {
                    dropStatus = modifier.removeValue(oldContainer, refName, parameter);
                }
                return dropStatus;
            }
            return super.caseParameter(parameter);
        }

        @Override
        public Status caseStateMachine(StateMachine stateMachine) {
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                return handleBehavioredClassifier(stateMachine, modifier);
            }
            return super.caseStateMachine(stateMachine);
        }

        @Override
        public Status caseProtocolStateMachine(ProtocolStateMachine protocolStateMachine) {
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                return handleBehavioredClassifier(protocolStateMachine, modifier);
            }
            return super.caseProtocolStateMachine(protocolStateMachine);
        }

        private Status handleBehavioredClassifier(BehavioredClassifier behavioredClassifier,
                IFeatureModifier modifier) {
            Status dropStatus = new Status(State.FAILED, null);
            String refName = behavioredClassifier.eContainmentFeature().getName();
            if (newContainer instanceof org.eclipse.uml2.uml.BehavioredClassifier
                    && !(newContainer instanceof Collaboration)) {
                dropStatus = modifier.addValue(newContainer,
                        UMLPackage.eINSTANCE.getBehavioredClassifier_OwnedBehavior().getName(), behavioredClassifier);
            } else if (newContainer instanceof Package) {
                dropStatus = modifier.addValue(newContainer,
                        UMLPackage.eINSTANCE.getPackage_PackagedElement().getName(), behavioredClassifier);
            }
            if (State.DONE == dropStatus.getState()) {
                dropStatus = modifier.removeValue(oldContainer, refName, behavioredClassifier);
            }
            return dropStatus;
        }

        @Override
        public Status caseInformationItem(InformationItem droppedInformationItem) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                String refName = droppedInformationItem.eContainmentFeature().getName();
                dropStatus = modifier.removeValue(oldContainer, refName, droppedInformationItem);
                if (State.DONE == dropStatus.getState()) {
                    if (newContainer instanceof org.eclipse.uml2.uml.Class) {
                        dropStatus = modifier.addValue(newContainer, NESTED_CLASSIFIER_REF, droppedInformationItem);
                    } else if (newContainer instanceof Package) {
                        dropStatus = modifier.addValue(newContainer, PACKAGED_ELEMENT_REF, droppedInformationItem);
                    }
                }
                return dropStatus;
            }
            return super.caseInformationItem(droppedInformationItem);
        }

        @Override
        public Status caseCollaboration(Collaboration droppedCollaboration) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                String refName = droppedCollaboration.eContainmentFeature().getName();
                dropStatus = modifier.removeValue(oldContainer, refName, droppedCollaboration);
                if (State.DONE == dropStatus.getState()) {
                    if (newContainer instanceof org.eclipse.uml2.uml.Class) {
                        dropStatus = modifier.addValue(newContainer, NESTED_CLASSIFIER_REF, droppedCollaboration);
                    } else if (newContainer instanceof Package) {
                        dropStatus = modifier.addValue(newContainer, PACKAGED_ELEMENT_REF, droppedCollaboration);
                    }
                }
                return dropStatus;
            }
            return super.caseCollaboration(droppedCollaboration);
        }

        @Override
        public Status caseProperty(Property droppedProperty) {
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                EObject oldSemanticContainer = droppedProperty.eContainer();
                if (oldSemanticContainer != null) {
                    String refName = droppedProperty.eContainingFeature().getName();
                    EObject newSemanticContainer = getPropertyNewSemanticContainer();
                    Status dropStatus = null;
                    if (newSemanticContainer != null) {
                        dropStatus = modifier.removeValue(oldSemanticContainer, refName, droppedProperty);
                        if (State.DONE == dropStatus.getState()) {
                            dropStatus = modifier.addValue(newSemanticContainer, OWNED_ATTRIBUTE, droppedProperty);
                        }
                    } else {
                        dropStatus = Status.createFailingStatus(
                                "Container should be a Structured Classifier or a Typed Property.");
                    }
                    return dropStatus;
                }
            }
            return super.caseProperty(droppedProperty);
        }

        private EObject getPropertyNewSemanticContainer() {
            EObject newSemanticContainer = null;
            if (newContainer instanceof Property) {
                Type type = ((Property) newContainer).getType();
                if (type instanceof StructuredClassifier) {
                    newSemanticContainer = type;
                }
            } else if (newContainer instanceof StructuredClassifier) {
                newSemanticContainer = newContainer;
            }
            return newSemanticContainer;
        }
    }

}
