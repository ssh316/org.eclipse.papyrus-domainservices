/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class ElementDomainBasedEdgeSourceProvider implements IDomainBasedEdgeSourceProvider {

    @Override
    public EObject getSource(EObject semanticElement) {
        return new EdgeProviderSwitch().doSwitch(semanticElement);
    }

    private static class EdgeProviderSwitch extends UMLSwitch<EObject> {

        @Override
        public EObject caseAssociation(Association object) {
            // Source of association is the type of the target property
            return object.getMemberEnds().get(1).getType();
        }

        @Override
        public EObject caseConnector(Connector connector) {
            // Simplified version for the moment
            return connector.getEnds().stream()//
                    .filter(end -> end.getRole() != null)//
                    .map(end -> end.getRole())//
                    .findFirst().orElse(null);
        }

        @Override
        public EObject caseGeneralization(Generalization object) {
            return object.getSpecific();
        }

        @Override
        public EObject caseInclude(Include object) {
            return object.getIncludingCase();
        }

        @Override
        public EObject caseSubstitution(Substitution object) {
            return object.getSubstitutingClassifier();
        }

        @Override
        public EObject caseDependency(Dependency dependency) {
            EList<NamedElement> clients = dependency.getClients();
            if (clients.isEmpty()) {
                return null;
            } else {
                return clients.get(0);
            }
        }

        @Override
        public EObject caseInformationFlow(InformationFlow informationFlow) {
            return informationFlow.getInformationSources().stream().findFirst().orElse(null);
        }

        @Override
        public EObject casePackageMerge(PackageMerge object) {
            return object.getReceivingPackage();
        }

        @Override
        public EObject casePackageImport(PackageImport object) {
            return object.getImportingNamespace();
        }

        @Override
        public EObject caseTransition(Transition transition) {
            return transition.getSource();
        }

        @Override
        public EObject caseExtend(Extend extend) {
            return extend.getExtension();
        }
    }
}
