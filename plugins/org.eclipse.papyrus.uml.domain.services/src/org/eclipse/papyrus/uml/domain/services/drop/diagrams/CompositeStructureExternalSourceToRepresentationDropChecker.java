/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.UMLPackage;

public class CompositeStructureExternalSourceToRepresentationDropChecker
        implements IExternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        final CheckStatus result;
        switch (elementToDrop.eClass().getClassifierID()) {
        case UMLPackage.ACTIVITY:
            result = handleClass(newSemanticContainer);
            break;
        case UMLPackage.CLASS:
            result = handleClass(newSemanticContainer);
            break;
        case UMLPackage.COLLABORATION:
            result = handleCollaboration(newSemanticContainer);
            break;
        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    private CheckStatus handleClass(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof StructuredClassifier) && !(newSemanticContainer instanceof Package)
                && !(newSemanticContainer instanceof Property)) {
            result = CheckStatus
                    .no("Class can only be drag and drop on a Structured Classifier or Package or Property.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleCollaboration(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof StructuredClassifier) && !(newSemanticContainer instanceof Package)
                && !(newSemanticContainer instanceof Property)) {
            result = CheckStatus
                    .no("Collaboration can only be drag and drop on a Structured Classifier or Package or Property.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

}
