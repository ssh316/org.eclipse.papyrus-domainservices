/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.destroy;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.services.CollaborationHelper;
import org.eclipse.papyrus.uml.domain.services.services.DurationConstraintHelper;
import org.eclipse.papyrus.uml.domain.services.services.DurationObservationHelper;
import org.eclipse.papyrus.uml.domain.services.services.OccurrenceSpecificationHelper;
import org.eclipse.papyrus.uml.domain.services.services.TimeConstraintHelper;
import org.eclipse.papyrus.uml.domain.services.services.TimeObservationHelper;
import org.eclipse.papyrus.uml.domain.services.services.UMLService;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.DestructionOccurrenceSpecification;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionFragment;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageEnd;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OccurrenceSpecification;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PartDecomposition;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.TimeObservation;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Switch class used to delegate "before destroy dependence" actions according
 * to the type of the object to delete.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 */
public class ElementDependencyCollector implements IDestroyerDependencyCollector {

    /**
     * Adapter used to get inverse references.
     */
    private final ECrossReferenceAdapter crossReferenceAdapter;

    /**
     * Constructor.
     * 
     * @param theCrossReferenceAdapter
     *                                 an adapter used to get inverse references
     */
    public ElementDependencyCollector(ECrossReferenceAdapter theCrossReferenceAdapter) {
        super();
        this.crossReferenceAdapter = theCrossReferenceAdapter;
    }

    @Override
    public Set<EObject> collectDependencies(EObject source) {
        DestroyDependencyCollectorSwitch collector = new DestroyDependencyCollectorSwitch(crossReferenceAdapter);
        collector.doSwitch(source);
        return collector.getDependentsToRemove();
    }

    static class DestroyDependencyCollectorSwitch extends UMLSwitch<Void> {

        /**
         * Adapter used to get inverse references.
         */
        private final ECrossReferenceAdapter crossReferenceAdapter;
        /**
         * Set of dependences to remove.
         */
        private final Set<EObject> dependentsToRemove = new HashSet<>();

        /**
         * @param crossReferenceAdapter
         */
        DestroyDependencyCollectorSwitch(ECrossReferenceAdapter crossReferenceAdapter) {
            super();
            this.crossReferenceAdapter = crossReferenceAdapter;
        }

        /**
         * Action to launch before deleting a {@link NamedElement}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.NamedElementHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * <pre>
         * This method deletes {@link DirectedRelationship} related to the named element (source or target).
         * </pre>
         * 
         * @param namedElementToDelete
         *                             the {@link NamedElement} to remove
         * 
         */
        @Override
        public Void caseNamedElement(NamedElement namedElementToDelete) {
            Iterator<DirectedRelationship> srcRelationhipsIt = namedElementToDelete
                    .getSourceDirectedRelationships(UMLPackage.eINSTANCE.getDependency()).iterator();
            while (srcRelationhipsIt.hasNext()) {
                DirectedRelationship directedRelationship = srcRelationhipsIt.next();

                // If all sources from the directed relationship are to be destroyed, add the
                // relationship destruction
                if (directedRelationship.getSources().contains(namedElementToDelete)) {
                    dependentsToRemove.add(directedRelationship);
                }
            }
            Iterator<DirectedRelationship> tgtRelationhipsIt = namedElementToDelete
                    .getTargetDirectedRelationships(UMLPackage.eINSTANCE.getDependency()).iterator();
            while (tgtRelationhipsIt.hasNext()) {
                DirectedRelationship directedRelationship = tgtRelationhipsIt.next();

                // If all sources from the directed relationship are to be destroyed, add the
                // relationship destruction
                if (directedRelationship.getTargets().contains(namedElementToDelete)) {
                    dependentsToRemove.add(directedRelationship);
                }
            }
            return super.caseNamedElement(namedElementToDelete);
        }

        /**
         * Action to launch before deleting a {@link ConnectorEnd}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.ConnectorEndHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest))}
         * 
         * <pre>
         * It deletes the related Connector in case this connector only has less than 2 ends left.
         * </pre>
         * 
         * @param connectorEndToDelete
         *                             the {@link NamedElement} to remove
         */
        @Override
        public Void caseConnectorEnd(ConnectorEnd connectorEndToDelete) {
            Connector connector = (Connector) connectorEndToDelete.getOwner();
            if (connector.getEnds().size() <= 2) {
                dependentsToRemove.add(connector);
            }
            return super.caseConnectorEnd(connectorEndToDelete);
        }

        /**
         * Action to launch before deleting a {@link Property}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.PropertyHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * @param propertyToDelete
         *                         the property to delete
         * 
         */
        @Override
        public Void caseProperty(Property propertyToDelete) {
            EReference[] refs = null;

            // Get related ConnectorEnd to be destroyed with the property
            // Possible references from ConnectorEnd to Property (or Port)
            refs = new EReference[] { UMLPackage.eINSTANCE.getConnectorEnd_Role(),
                    UMLPackage.eINSTANCE.getConnectorEnd_PartWithPort() };
            Collection<EObject> connectorEndRefs = UMLService.getReferencers(propertyToDelete, refs,
                    this.crossReferenceAdapter);
            dependentsToRemove.addAll(connectorEndRefs);

            // Get possible associations using this Property as end
            refs = new EReference[] { UMLPackage.eINSTANCE.getAssociation_MemberEnd() };
            Collection<EObject> associationRefs = UMLService.getReferencers(propertyToDelete, refs,
                    this.crossReferenceAdapter);
            for (EObject association : associationRefs) {

                // Test the number of remaining ends considering the dependents elements
                // deletion in progress
                List<Property> remainingMembers = new ArrayList<>();
                remainingMembers.addAll(((Association) association).getMemberEnds());
                if (remainingMembers.size() <= 2) {
                    dependentsToRemove.add(association);
                }
            }

            return super.caseProperty(propertyToDelete);
        }

        /**
         * Action to launch before deleting a {@link Classifier}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.ClassifierHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * <pre>
         * This method deletes :
         * - Generalization related to the Classifier (source or target).
         * - Association related to the Classifier (source or target type).
         * </pre>
         * 
         * @param classifierToDelete
         *                           the {@link Classifier} to remove
         * 
         */
        @Override
        public Void caseClassifier(Classifier classifierToDelete) {
            // Get related generalizations
            dependentsToRemove.addAll(
                    classifierToDelete.getSourceDirectedRelationships(UMLPackage.eINSTANCE.getGeneralization()));
            dependentsToRemove.addAll(
                    classifierToDelete.getTargetDirectedRelationships(UMLPackage.eINSTANCE.getGeneralization()));

            // Get related association for this classifier, then delete member ends for
            // which this classifier is the type.
            for (Association association : classifierToDelete.getAssociations()) {
                for (Property end : association.getMemberEnds()) {
                    if (end.getType() == classifierToDelete) {
                        dependentsToRemove.add(association);
                    }
                }
            }
            return super.caseClassifier(classifierToDelete);
        }

        @Override
        public Void casePackage(Package object) {
            crossReferenceAdapter.getInverseReferences(object, true).stream()
                    .filter(s -> s.getEStructuralFeature() == UMLPackage.eINSTANCE.getPackageImport_ImportedPackage()
                            || s.getEStructuralFeature() == UMLPackage.eINSTANCE.getPackageMerge_MergedPackage())
                    .map(s -> s.getEObject()).forEach(dependentsToRemove::add);
            return super.casePackage(object);
        }

        /**
         * Action to launch before deleting a {@link Collaboration}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.CollaborationHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * @param classifierToDelete
         *                           the {@link Collaboration} to remove
         * 
         */
        @Override
        public Void caseCollaboration(Collaboration collaborationToDelete) {
            dependentsToRemove.addAll(CollaborationHelper.getRelatedRoleBindings(collaborationToDelete, null,
                    this.crossReferenceAdapter));
            return super.caseCollaboration(collaborationToDelete);
        }

        /**
         * Action to launch before deleting a
         * {@link DestructionOccurrenceSpecification}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.DestructionOccurrenceSpecificationEditHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * @param destructionToDelete
         *                            the {@link DestructionOccurrenceSpecification} to
         *                            remove
         */
        @Override
        public Void caseDestructionOccurrenceSpecification(DestructionOccurrenceSpecification destructionToDelete) {
            Interaction interaction = OccurrenceSpecificationHelper.getInteraction(destructionToDelete);

            Stream<TimeConstraint> timeConstraints = OccurrenceSpecificationHelper.getTimeConstraints(interaction,
                    destructionToDelete);
            Stream<TimeObservation> timeObservations = OccurrenceSpecificationHelper.getTimeObservations(interaction,
                    destructionToDelete);

            dependentsToRemove.addAll(Stream.concat(timeConstraints, timeObservations).collect(toList()));
            return super.caseDestructionOccurrenceSpecification(destructionToDelete);

        }

        /**
         * Action to launch before deleting a {@link ExecutionSpecification}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.ExecutionSpecificationHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * @param esToDelete
         *                   the {@link ExecutionSpecification} to remove
         */
        @Override
        public Void caseExecutionSpecification(ExecutionSpecification esToDelete) {
            // Check whether start - finish referenced OccurrenceSpecification should be
            // added to the dependents list
            OccurrenceSpecification osStart = esToDelete.getStart();
            if (OccurrenceSpecificationHelper.shouldDestroyOccurrenceSpecification(esToDelete, osStart,
                    this.crossReferenceAdapter) && (!(osStart instanceof MessageEnd))) {
                dependentsToRemove.add(osStart);
            }

            OccurrenceSpecification osFinish = esToDelete.getFinish();
            if (OccurrenceSpecificationHelper.shouldDestroyOccurrenceSpecification(esToDelete, osFinish,
                    this.crossReferenceAdapter) && (!(osFinish instanceof MessageEnd))) {
                dependentsToRemove.add(osFinish);
            }

            return super.caseExecutionSpecification(esToDelete);
        }

        /**
         * Action to launch before deleting a {@link Lifeline}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.LifelineHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * @param lifelineToDelete
         *                         the {@link Lifeline} to remove
         */
        @Override
        public Void caseLifeline(Lifeline lifelineToDelete) {
            for (InteractionFragment ift : lifelineToDelete.getCoveredBys()) {
                // Destroy covered ExecutionSpecification
                if (ift instanceof ExecutionSpecification) {
                    dependentsToRemove.add(ift);
                }

                // Destroy related Message
                // Destroy related Message
                if ((ift instanceof MessageOccurrenceSpecification)
                        && (((MessageOccurrenceSpecification) ift).getMessage() != null)) {
                    dependentsToRemove.add(((MessageOccurrenceSpecification) ift).getMessage());
                }

                // Destroy covered OccurrenceSpecification
                if (ift instanceof OccurrenceSpecification) {
                    dependentsToRemove.add(ift);
                }
            }

            // Destroy decomposed lifelines
            PartDecomposition decomposition = lifelineToDelete.getDecomposedAs();
            if (decomposition != null
                    && UMLService.isOnlyUsage(decomposition, lifelineToDelete, this.crossReferenceAdapter)) {
                dependentsToRemove.add(decomposition);
            }
            return super.caseLifeline(lifelineToDelete);
        }

        /**
         * Action to launch before deleting a {@link OccurrenceSpecification}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.OccurrenceSpecificationHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * <pre>
         * Add dependents to destroy :
         * - related time elements
         * - linked general ordering
         * </pre>
         * 
         * @param osToDelete
         *                   the {@link OccurrenceSpecification} to remove
         */
        @Override
        public Void caseOccurrenceSpecification(OccurrenceSpecification osToDelete) {
            // look for all Execution that references this Occurrence specification
            InteractionFragment containerPackage = (InteractionFragment) osToDelete.getOwner();
            if (containerPackage != null) {
                Iterator<EObject> contentIterator = containerPackage.eAllContents();
                while (contentIterator.hasNext()) {
                    EObject currentEObject = contentIterator.next();
                    if (currentEObject instanceof Message) {
                        Message m = (Message) currentEObject;
                        if (osToDelete.equals(m.getSendEvent())) {
                            dependentsToRemove.add(m);
                            if (m.getReceiveEvent() != null) {
                                dependentsToRemove.add(m.getReceiveEvent());
                            }
                        }
                        if (osToDelete.equals(m.getReceiveEvent())) {
                            dependentsToRemove.add(m);
                            if (m.getSendEvent() != null) {
                                dependentsToRemove.add(m.getSendEvent());
                            }
                        }
                    }
                    if (currentEObject instanceof ExecutionSpecification) {
                        ExecutionSpecification exec = (ExecutionSpecification) currentEObject;
                        if (osToDelete.equals(exec.getStart())) {
                            dependentsToRemove.add(exec);
                            if (exec.getFinish() != null && !(exec.getFinish() instanceof MessageEnd)) {
                                dependentsToRemove.add(exec.getFinish());
                            }
                        }
                        if (osToDelete.equals(exec.getFinish())) {
                            dependentsToRemove.add(exec);
                            if (exec.getStart() != null && !(exec.getStart() instanceof MessageEnd)) {
                                dependentsToRemove.add(exec.getStart());
                            }
                        }
                    }
                }
            }
            // delete linked time elements
            dependentsToRemove
                    .addAll(TimeObservationHelper.getTimeObservations(osToDelete, this.crossReferenceAdapter));
            dependentsToRemove
                    .addAll(TimeConstraintHelper.getTimeConstraintsOn(osToDelete, this.crossReferenceAdapter));
            dependentsToRemove.addAll(
                    DurationObservationHelper.getDurationObservationsOn(osToDelete, this.crossReferenceAdapter));
            dependentsToRemove
                    .addAll(DurationConstraintHelper.getDurationConstraintsOn(osToDelete, this.crossReferenceAdapter));

            // delete linked general ordering
            /**
             * Note: GeneralOrdering should be necessarily removed because the opposite
             * references 'GeneralOrdering::before[1]' and 'GeneralOrdering::after[1]' which
             * designate this OccurrenceSpecification are mandatory
             */
            dependentsToRemove.addAll(osToDelete.getToBefores());
            dependentsToRemove.addAll(osToDelete.getToAfters());

            return super.caseOccurrenceSpecification(osToDelete);
        }

        /**
         * In the StateMachine, incoming and outgoing transitions are removed with the
         * Vertex (ex: State, PseudoState and InitialState)
         */
        @Override
        public Void caseVertex(Vertex vertex) {
            dependentsToRemove.addAll(vertex.getIncomings());
            dependentsToRemove.addAll(vertex.getOutgoings());
            return super.caseVertex(vertex);
        }

        /**
         * Action to launch before deleting a {@link Association}. Copy from
         * {@link org.eclipse.papyrus.uml.service.types.helper.advice.AssociationEditHelperAdvice.getBeforeDestroyDependentsCommand(DestroyDependentsRequest)}
         * 
         * @param association
         *                    the association to delete
         * 
         */
        @Override
        public Void caseAssociation(Association association) {

            EList<Property> ownedEnds = association.getOwnedEnds();
            for (Property end : association.getMemberEnds()) {
                if (ownedEnds.contains(end)) {
                    dependentsToRemove.add(end);
                } else if (end.getType() != null) {
                    // when user set the source or target property (not owned by association) type
                    // to null, the association is removed (because it cannot be defined without a
                    // source or target type) but we do not remove the source or target property
                    // with the type null.
                    dependentsToRemove.add(end);
                }
            }
            return super.caseAssociation(association);
        }

        @Override
        public Void caseUseCase(UseCase useCase) {
            // Delete related includes
            dependentsToRemove.addAll(useCase.getSourceDirectedRelationships(UMLPackage.eINSTANCE.getInclude()));
            dependentsToRemove.addAll(useCase.getTargetDirectedRelationships(UMLPackage.eINSTANCE.getInclude()));
            // Delete related extends
            dependentsToRemove.addAll(useCase.getSourceDirectedRelationships(UMLPackage.eINSTANCE.getExtend()));
            dependentsToRemove.addAll(useCase.getTargetDirectedRelationships(UMLPackage.eINSTANCE.getExtend()));
            return super.caseUseCase(useCase);
        }

        public Set<EObject> getDependentsToRemove() {
            return dependentsToRemove;
        }
    }

}
