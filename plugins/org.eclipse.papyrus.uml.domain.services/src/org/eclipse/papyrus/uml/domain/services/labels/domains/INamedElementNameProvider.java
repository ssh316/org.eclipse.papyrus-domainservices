/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import org.eclipse.uml2.uml.NamedElement;

/**
 * Interface used to display element name in the element label. For example, we
 * can display simple name or internationalized name or qualified name.
 * 
 * @author Jessy MALLET
 */
public interface INamedElementNameProvider {

    /**
     * Get the name to display.
     * 
     * @param element
     *                element with the name to display
     * @return the name to display.
     */
    String getName(NamedElement element);
}
