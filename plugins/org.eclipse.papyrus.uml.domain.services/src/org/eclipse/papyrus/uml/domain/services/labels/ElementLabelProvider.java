/*******************************************************************************
 * Copyright (c) 2022,2023 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import static org.eclipse.papyrus.uml.domain.services.labels.LabelUtils.getNonNullString;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EOL;

import java.util.function.Function;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.labels.domains.CollaborationUseLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.DefaultNamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.INamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.ParameterLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.PropertyLabelHelper;
import org.eclipse.papyrus.uml.domain.services.labels.domains.VisibilityLabelHelper;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.ValueSpecification;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Object in charge of providing a label for a semantic element.
 *
 * @author Arthur Daussy
 */
public class ElementLabelProvider implements IViewLabelProvider {

    private UMLSwitch<String> nameRenderer;

    private final Function<EObject, String> prefixLabelProvider;

    private final Function<EObject, String> keywordLabelProvider;

    /**
     * Constructor.
     * 
     * @param prefixLabelProvider
     *                             provider of label prefix
     * @param keywordLabelProvider
     *                             provider of keyword prefix
     * @param elementNameProvider
     *                             provider of element name
     */
    public ElementLabelProvider(Function<EObject, String> prefixLabelProvider,
            Function<EObject, String> keywordLabelProvider, INamedElementNameProvider elementNameProvider) {
        super();
        if (prefixLabelProvider != null) {
            this.prefixLabelProvider = prefixLabelProvider;
        } else {
            this.prefixLabelProvider = e -> null;
        }
        if (keywordLabelProvider != null) {
            this.keywordLabelProvider = keywordLabelProvider;
        } else {
            this.keywordLabelProvider = e -> null;
        }
        INamedElementNameProvider nameProvider = elementNameProvider;
        if (elementNameProvider == null) {
            nameProvider = new DefaultNamedElementNameProvider();
        }
        nameRenderer = new ElementLabelProviderSwitch(nameProvider);
    }

    public static ElementLabelProvider buildDefault() {
        return new ElementLabelProvider(new StereotypeLabelPrefixProvider(), new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
    }

    /**
     * Gets the label for the given element.
     *
     * @param element
     *                an element
     * @return a label (never returns <code>null</code> instead return an empty
     *         string)
     */
    @Override
    public String getLabel(EObject element) {
        if (element == null) {
            return ""; //$NON-NLS-1$
        }

        StringBuilder label = new StringBuilder();

        // add keywords
        String keyword = keywordLabelProvider.apply(element);
        if (keyword != null && !keyword.isBlank()) {
            label.append(keyword);
        }

        // add prefix
        String prefix = prefixLabelProvider.apply(element);
        if (prefix != null && !prefix.isBlank()) {
            if (label.length() > 0) {
                label.append(EOL);
            }
            label.append(prefix);
        }

        // add element name
        String baseLabel = this.nameRenderer.doSwitch(element);
        if (baseLabel != null && !baseLabel.isBlank()) {
            if (label.length() > 0) {
                label.append(EOL);
            }
            label.append(baseLabel);
        }

        return label.toString();
    }

    static final class ElementLabelProviderSwitch extends UMLSwitch<String> {

        private static final String NATURAL = "NATURAL";

        private static final String NULL_CONSTRAINT = "<NULL Constraint>";

        private CollaborationUseLabelHelper collaborationUseLabelHelper;

        private PropertyLabelHelper propertyLabelHelper;

        private ParameterLabelHelper parameterLabelHelper;

        private INamedElementNameProvider namedElementNameProvider;

        private VisibilityLabelHelper visibilityLabelHelper;

        ElementLabelProviderSwitch() {
            this(null);
        }

        ElementLabelProviderSwitch(INamedElementNameProvider namedElementNameProvider) {
            super();
            this.namedElementNameProvider = namedElementNameProvider;
            this.visibilityLabelHelper = new VisibilityLabelHelper();
            collaborationUseLabelHelper = new CollaborationUseLabelHelper(namedElementNameProvider,
                    visibilityLabelHelper);
            propertyLabelHelper = new PropertyLabelHelper(false, true, namedElementNameProvider, visibilityLabelHelper);
            parameterLabelHelper = new ParameterLabelHelper(false, true, namedElementNameProvider);
        }

        @Override
        public String caseCollaborationUse(CollaborationUse collaborationUse) {
            return this.collaborationUseLabelHelper.getLabel(collaborationUse);
        }

        @Override
        public String caseComment(Comment comment) {
            return comment.getBody();
        }

        @Override
        public String caseInformationFlow(InformationFlow flow) {
            String result = "";
            if (flow != null) {
                result = getConveyeds(flow.getConveyeds());
                String flowName = this.namedElementNameProvider.getName(flow);
                if (flowName != null && !flowName.isBlank()) {
                    if (!result.isBlank()) {
                        result += UMLCharacters.EOL;
                    }
                    result += flowName;
                }
            }
            return result;
        }

        private String getConveyeds(EList<Classifier> conveyeds) {
            String result = "";
            if (!conveyeds.isEmpty()) {
                result += conveyeds.stream().map(NamedElement::getName).collect(Collectors.joining(", "));
            }
            return result;
        }

        public String caseNamedElement(NamedElement object) {
            return this.namedElementNameProvider.getName(object);
        }

        @Override
        public String caseRegion(Region object) {
            return "";
        }

        @Override
        public String caseProperty(Property property) {
            return this.propertyLabelHelper.getLabel(property);
        }

        @Override
        public String caseParameter(Parameter parameter) {
            return this.parameterLabelHelper.getLabel(parameter);
        }

        /**
         * Copied from
         * org.eclipse.papyrus.uml.diagram.common.parser.ConstraintParser.getEditString(IAdaptable,
         * int)
         */
        @Override
        public String caseConstraint(Constraint constraint) {
            StringBuilder constLabel = new StringBuilder();
            String body = UMLCharacters.EMPTY;
            String lang = UMLCharacters.EMPTY;
            ValueSpecification valueSpec = constraint.getSpecification();
            constLabel.append(constraint.getName());
            constLabel.append(UMLCharacters.EOL);
            constLabel.append(UMLCharacters.OPEN_BRACKET);
            if (valueSpec == null) {
                constLabel.append(NULL_CONSTRAINT);
            } else if (valueSpec instanceof OpaqueExpression) {
                OpaqueExpression opaqueEsp = (OpaqueExpression) valueSpec;
                if (!opaqueEsp.getBodies().isEmpty() && !opaqueEsp.getLanguages().isEmpty()) {
                    body = opaqueEsp.getBodies().get(0);
                    lang = opaqueEsp.getLanguages().get(0);
                    constLabel.append(UMLCharacters.OPEN_BRACKET);
                    constLabel.append(lang);
                    constLabel.append(UMLCharacters.CLOSE_BRACKET);
                    constLabel.append(UMLCharacters.SPACE);
                    constLabel.append(body);
                } else {
                    constLabel.append(UMLCharacters.OPEN_BRACKET);
                    constLabel.append(NATURAL);
                    constLabel.append(UMLCharacters.CLOSE_BRACKET);
                    constLabel.append(UMLCharacters.SPACE);
                }
            }
            constLabel.append(UMLCharacters.CLOSE_BRACKET);
            return constLabel.toString();
        }

        @Override
        public String caseOperation(Operation operation) {
            String visibility = getNonNullString(visibilityLabelHelper.getVisibilityAsSign(operation))
                    + UMLCharacters.SPACE;
            String parameters = operation.getOwnedParameters().stream().map(p -> p.getLabel())
                    .collect(Collectors.joining(UMLCharacters.COMMA + UMLCharacters.SPACE,
                            UMLCharacters.OPEN_PARENTHESE, UMLCharacters.CLOSE_PARENTHESE));
            return visibility + this.namedElementNameProvider.getName(operation) + UMLCharacters.SPACE + parameters;
        }
    }
}
