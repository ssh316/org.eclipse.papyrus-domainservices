/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.drop;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.status.Status;

/**
 * Provider of behavior when dropping diagram element to an other one.
 * 
 * @author Jessy MALLET
 *
 */
public interface IInternalSourceToRepresentationDropBehaviorProvider {

    /**
     * Drops a diagram element on another one.
     * 
     * @param droppedElement
     *                        the element being dropped
     * @param oldContainer
     *                        the previous semantic container of the droppedElement
     * @param newContainer
     *                        the semantic target of the drop
     * @param crossRef
     *                        a {@link ECrossReferenceAdapter}
     * @param editableChecker
     *                        a {@link IEditableChecker}
     * @return a Status of the drop
     */
    Status drop(EObject droppedElement, EObject oldContainer, EObject newContainer, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker);

}
