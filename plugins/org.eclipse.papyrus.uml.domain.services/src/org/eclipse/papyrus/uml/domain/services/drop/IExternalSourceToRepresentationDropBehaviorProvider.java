/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.drop;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.status.Status;

/**
 * Provider of behavior when dropping semantic elements (from Explorer) to a
 * diagram element.
 * 
 * @author Arthur Daussy
 *
 */
public interface IExternalSourceToRepresentationDropBehaviorProvider {
    /**
     * Drops a semantic element on another.
     * 
     * @param droppedElement
     *                        the element being dropped
     * @param target
     *                        the semantic target of the drop
     * @param crossRef
     *                        a {@link ECrossReferenceAdapter}
     * @param editableChecker
     *                        a {@link IEditableChecker}
     * @return a Status of the drop
     */
    Status drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker);

}
