/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Reconnect target behavior provider of a semantic element.
 * 
 * @author Jessy MALLET
 *
 */
public class ElementDomainBasedEdgeReconnectTargetBehaviorProvider
        implements IDomainBasedEdgeReconnectTargetBehaviorProvider {

    @Override
    public CheckStatus reconnectTarget(EObject elementToReconnect, EObject oldTarget, EObject newTarget) {
        if (elementToReconnect == null || oldTarget == null || newTarget == null) {
            return CheckStatus.no(
                    MessageFormat.format("Invalid input for reconnection (element ={0} oldTarget ={1} newTarget = {2})", //$NON-NLS-1$
                            elementToReconnect, oldTarget, newTarget));
        }
        return new ReconnectTargetBehaviorProviderSwitch(oldTarget, newTarget).doSwitch(elementToReconnect);
    }

    static class ReconnectTargetBehaviorProviderSwitch extends UMLSwitch<CheckStatus> {

        private final EObject oldTarget;

        private final EObject newTarget;

        ReconnectTargetBehaviorProviderSwitch(EObject oldTarget, EObject newTarget) {
            super();
            this.oldTarget = oldTarget;
            this.newTarget = newTarget;
        }

        @Override
        public CheckStatus casePackageMerge(PackageMerge object) {
            object.setMergedPackage((Package) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus casePackageImport(PackageImport packImport) {
            packImport.setImportedPackage((Package) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseDependency(Dependency dependency) {
            dependency.getSuppliers().remove(oldTarget);
            dependency.getSuppliers().add((NamedElement) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseTransition(Transition transition) {
            transition.setTarget((Vertex) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseGeneralization(Generalization generalization) {
            generalization.setGeneral((Classifier) newTarget);
            return CheckStatus.YES;
        }

        public CheckStatus caseAssociation(Association association) {
            Property sourceprop = association.getMemberEnds().get(0);
            sourceprop.setType((Classifier) newTarget);
            return CheckStatus.YES;
        }

        @Override
        public CheckStatus caseInclude(Include include) {
            include.setAddition((UseCase) newTarget);
            return CheckStatus.YES;
        }

    }

}
