/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Gather helper methods around the UML meta model.
 *
 * @author Arthur Daussy
 */
public final class UMLHelper {

    private static final String UML_PACKAGE_PREFIX = "uml::"; //$NON-NLS-1$

    private UMLHelper() {
    }

    /**
     * Gets the {@link EClass} from the {@link UMLPackage} using the simple or
     * qualified name ("Class" vs "uml::Class").
     *
     * @param type
     *             the searched type
     * @return a {@link EClassifier} or <code>null</code>
     */
    public static EClassifier toEClass(String type) {
        if (type != null && type.startsWith(UML_PACKAGE_PREFIX)) {
            return toEClass(type.replace(UML_PACKAGE_PREFIX, "")); //$NON-NLS-1$
        }
        return UMLPackage.eINSTANCE.getEClassifier(type);
    }

}
