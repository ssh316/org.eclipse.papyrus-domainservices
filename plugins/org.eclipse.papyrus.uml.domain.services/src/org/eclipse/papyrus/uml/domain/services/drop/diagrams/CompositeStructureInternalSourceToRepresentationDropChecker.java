/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.UMLPackage;

public class CompositeStructureInternalSourceToRepresentationDropChecker
        implements IInternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        final CheckStatus result;
        switch (elementToDrop.eClass().getClassifierID()) {
        case UMLPackage.CLASS:
            result = handleClass(newSemanticContainer);
            break;
        case UMLPackage.COLLABORATION:
            result = handleCollaboration(newSemanticContainer);
            break;
        case UMLPackage.COMMENT:
            result = handleComment(newSemanticContainer);
            break;
        case UMLPackage.CONSTRAINT:
            result = handleConstraint(newSemanticContainer);
            break;
        case UMLPackage.INFORMATION_ITEM:
            result = handleInformationItem(newSemanticContainer);
            break;
        case UMLPackage.PROPERTY:
            result = handleProperty(newSemanticContainer);
            break;
        default:
            result = CheckStatus.YES;
            break;
        }
        return result;
    }

    private CheckStatus handleProperty(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof StructuredClassifier
                || (newSemanticContainer instanceof Property && ((Property) newSemanticContainer).getType() != null)
                        && !(newSemanticContainer instanceof Port))) {
            result = CheckStatus.no("Property can only be drag and drop on Structured Classifier or typed Property.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleClass(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof org.eclipse.uml2.uml.Class || newSemanticContainer instanceof Package)) {
            result = CheckStatus.no("Class can only be drag and drop on an other Class or Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleCollaboration(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof org.eclipse.uml2.uml.Class || newSemanticContainer instanceof Package)) {
            result = CheckStatus.no("Collaboration can only be drag and drop on Class or Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleComment(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Element) || newSemanticContainer instanceof Comment) {
            result = CheckStatus.no("Comment can only be drag and drop on an Element different of a Comment.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleConstraint(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof Namespace)) {
            result = CheckStatus.no("Constraint can only be drag and drop on a Namespace.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

    private CheckStatus handleInformationItem(EObject newSemanticContainer) {
        final CheckStatus result;
        if (!(newSemanticContainer instanceof org.eclipse.uml2.uml.Class || newSemanticContainer instanceof Package)) {
            result = CheckStatus.no("InformationItem can only be drag and drop on Class or Package.");
        } else {
            result = CheckStatus.YES;
        }
        return result;
    }

}
