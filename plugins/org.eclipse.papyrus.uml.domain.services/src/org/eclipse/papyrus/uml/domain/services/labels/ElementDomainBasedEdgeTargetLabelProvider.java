/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.labels.domains.DefaultNamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.INamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.labels.domains.MultiplicityLabelHelper;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Provider of a label for domain based edge on the target (label located at the
 * end of an edge).
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDomainBasedEdgeTargetLabelProvider implements IDomainBasedEdgeEndLabelProvider {

    private INamedElementNameProvider namedElementNameProvider;

    public ElementDomainBasedEdgeTargetLabelProvider(INamedElementNameProvider namedElementNameProvider) {
        this.namedElementNameProvider = namedElementNameProvider;
    }

    public static ElementDomainBasedEdgeTargetLabelProvider buildDefault() {
        return new ElementDomainBasedEdgeTargetLabelProvider(new DefaultNamedElementNameProvider());
    }

    @Override
    public String getLabel(EObject element, EObject semanticTarget) {
        String label = new ElementDomainBasedEdgeSourceLabelProviderSwitch(semanticTarget, namedElementNameProvider)
                .doSwitch(element);

        if (label == null) {
            label = "";
        }
        return label;
    }

    public static final class ElementDomainBasedEdgeSourceLabelProviderSwitch extends UMLSwitch<String> {

        private final EObject semanticTarget;

        private MultiplicityLabelHelper multiplicityLabelHelper;

        public ElementDomainBasedEdgeSourceLabelProviderSwitch(EObject semanticTarget,
                INamedElementNameProvider namedElementNameProvider) {
            super();
            this.semanticTarget = semanticTarget;
            this.multiplicityLabelHelper = new MultiplicityLabelHelper(namedElementNameProvider);
        }

        @Override
        public String caseConnector(Connector connector) {

            EList<ConnectorEnd> ends = connector.getEnds();
            if (ends.size() > 1) {
                if (semanticTarget != null) {
                    return ends.stream().skip(1).filter(e -> e.getRole() == semanticTarget)//
                            // Not perfect since it will not handle connector with more than 1 ConnectorEnd
                            // using the same element as role but does it really make sense?
                            .findFirst().map(e -> multiplicityLabelHelper.formatMultiplicityNoBrackets(e, false))
                            .orElse("");
                }

            }
            return super.caseConnector(connector);
        }

    }
}
