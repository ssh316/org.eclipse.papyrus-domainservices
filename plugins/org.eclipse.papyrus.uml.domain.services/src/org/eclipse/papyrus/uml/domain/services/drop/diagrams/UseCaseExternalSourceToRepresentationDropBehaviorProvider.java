/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class UseCaseExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public Status drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new UseCaseDropInsideRepresentationBehaviorProviderSwitch(target, crossRef, editableChecker)
                .doSwitch(droppedElement);
    }

    static class UseCaseDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<Status> {

        private final EObject target;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        UseCaseDropInsideRepresentationBehaviorProviderSwitch(EObject target, ECrossReferenceAdapter crossRef,
                IEditableChecker editableChecker) {
            super();
            this.target = target;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

    }

}
