/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.directedit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.junit.jupiter.api.Test;

public class ElementDirectEditValueConsumerTest extends AbstractUMLTest {

    /**
     * Check using the direct edit tool on a Comment set the body.
     */
    @Test
    public void checkComment() {
        Comment comment = create(Comment.class);
        comment.setBody("A body");
        CheckStatus status = new ElementDirectEditValueConsumer().consumeNewLabel(comment, "A new body");
        assertTrue(status.isValid());
        assertEquals("A new body", comment.getBody());

    }

    /**
     * Check using the direct edit tool on a NamedElement (here a comment) set the
     * name.
     */
    @Test
    public void checkNamedElement() {
        Component component = create(Component.class);
        component.setName("A component");
        CheckStatus status = new ElementDirectEditValueConsumer().consumeNewLabel(component, "A new Component name");
        assertTrue(status.isValid());
        assertEquals("A new Component name", component.getName());

    }
}
