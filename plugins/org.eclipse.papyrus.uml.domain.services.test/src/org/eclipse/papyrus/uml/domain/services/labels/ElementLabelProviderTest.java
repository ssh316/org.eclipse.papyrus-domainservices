/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import static org.eclipse.papyrus.uml.domain.services.EMFUtils.allContainedObjectOfType;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EOL;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_LEFT;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_RIGHT;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.TILDE;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.uml.domain.services.labels.domains.DefaultNamedElementNameProvider;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ProtocolStateMachine;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.VisibilityKind;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementLabelProvider}.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementLabelProviderTest extends AbstractUMLTest {

    /**
     * Model containing a UML Model with stereotype content.
     */
    public static final URI UML_MODEL_WITH_PROFILE = URI.createPlatformPluginURI(
            "/org.eclipse.papyrus.uml.domain.services.test/profile/UMLModelWithStandardProfile.uml", false);

    private static final String FLOW = "flow";
    private static final String C1_NAME = "c1";
    private static final String IF1 = "if1";
    private static final String SPACE = " ";
    private static final String CONST = "const";

    /**
     * Basic test case for {@link Usage} label. Prefix \u00ABuse\u00BB should
     * appear.
     */
    @Test
    public void testUsageLabels() {
        Usage usage = create(Usage.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "use" + ST_RIGHT, elementLabelProvider.getLabel(usage));
        usage.setName("u1");
        assertEquals(ST_LEFT + "use" + ST_RIGHT + EOL + "u1", elementLabelProvider.getLabel(usage));
    }

    /**
     * Basic test case for {@link Abstraction} label. Prefix \u00ABabstraction\u00BB
     * should appear.
     */
    @Test
    public void testAbstractionLabels() {
        Abstraction abstraction = create(Abstraction.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "abstraction" + ST_RIGHT, elementLabelProvider.getLabel(abstraction));
        abstraction.setName("ab1");
        assertEquals(ST_LEFT + "abstraction" + ST_RIGHT + EOL + "ab1", elementLabelProvider.getLabel(abstraction));
    }

    @Test
    public void testOnNull() {
        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals("", elementLabelProvider.getLabel(null)); //$NON-NLS-1$
    }

    @Test
    public void testOnClass() {
        var clazz = create(Class.class);
        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals("", elementLabelProvider.getLabel(clazz)); //$NON-NLS-1$

        String className = "Class éé"; //$NON-NLS-1$
        clazz.setName(className);
        assertEquals(className, elementLabelProvider.getLabel(clazz));
    }

    @Test
    public void testOnProperty() {

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        Property property = create(Property.class);
        property.setName("prop1"); //$NON-NLS-1$

        assertEquals(" + prop1: <Undefined> [1]", elementLabelProvider.getLabel(property)); //$NON-NLS-1$

        var type = create(Class.class);
        type.setName(C1_NAME);
        property.setType(type);

        assertEquals(" + prop1: c1 [1]", elementLabelProvider.getLabel(property)); //$NON-NLS-1$

        // More use case tested in the labels packages
    }

    @Test
    public void testOnPort() {

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        Port port = create(Port.class);
        port.setName("port1"); //$NON-NLS-1$
        port.setIsConjugated(true);

        assertEquals(" + port1: " + TILDE + "<Undefined> [1]", elementLabelProvider.getLabel(port)); //$NON-NLS-1$

        // More use case tested in testOnProperty
    }

    @Test
    public void testComment() {
        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        Comment comment = create(Comment.class);
        comment.setBody("some comment"); //$NON-NLS-1$

        assertEquals(elementLabelProvider.getLabel(comment), "some comment");
    }

    /**
     * Basic test case for a basic {@link Dependency} label such as
     * {@link Realization} label. None Prefix should appear.
     */
    @Test
    public void testRealizationLabels() {
        Realization realization = create(Realization.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals("", elementLabelProvider.getLabel(realization));
        realization.setName("r1");
        assertEquals("r1", elementLabelProvider.getLabel(realization));
    }

    /**
     * Basic test case for {@link Substitution} label. Prefix \u00ABsubstitute\u00BB
     * should appear.
     */
    @Test
    public void testSubstitutionLabels() {
        Substitution substitution = create(Substitution.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "substitute" + ST_RIGHT, elementLabelProvider.getLabel(substitution));
        substitution.setName("s1");
        assertEquals(ST_LEFT + "substitute" + ST_RIGHT + EOL + "s1", elementLabelProvider.getLabel(substitution));
    }

    /**
     * Basic test case for {@link Manifestation} label. Prefix \u00ABmanifest\u00BB
     * should appear.
     */
    @Test
    public void testManifestationLabels() {
        Manifestation manifestation = create(Manifestation.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "manifest" + ST_RIGHT, elementLabelProvider.getLabel(manifestation));
        manifestation.setName("m1");
        assertEquals(ST_LEFT + "manifest" + ST_RIGHT + EOL + "m1", elementLabelProvider.getLabel(manifestation));
    }

    /**
     * Basic test case for {@link InformationItem} label. Prefix
     * \u00ABInformation\u00BB should appear.
     */
    @Test
    public void testInformationItemLabels() {
        InformationItem informationItem = create(InformationItem.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "information" + UMLCharacters.ST_RIGHT, elementLabelProvider.getLabel(informationItem));
        informationItem.setName("ii1");
        assertEquals(ST_LEFT + "information" + UMLCharacters.ST_RIGHT + EOL + "ii1",
                elementLabelProvider.getLabel(informationItem));
    }

    @Test
    public void testStereotypeElementLabel() {
        ResourceSet rs = new ResourceSetImpl();
        Resource umlResource = rs.getResource(UML_MODEL_WITH_PROFILE, true);

        String classOneStereotypeName = "ClassOneStereotype";
        Class classOneStereotype = allContainedObjectOfType(umlResource, Class.class)
                .filter(c -> classOneStereotypeName.equals(c.getName())).findFirst().orElseThrow();

        ElementLabelProvider labelProvider = ElementLabelProvider.buildDefault();
        String label0 = labelProvider.getLabel(classOneStereotype);
        assertEquals(ST_LEFT + "Utility" + ST_RIGHT + EOL + classOneStereotypeName, label0);

        String classTwoStereotypeName = "ClassTwoStereotypes";
        Class classTwoStereotype = allContainedObjectOfType(umlResource, Class.class)
                .filter(c -> classTwoStereotypeName.equals(c.getName())).findFirst().orElseThrow();

        String label2 = labelProvider.getLabel(classTwoStereotype);
        assertEquals(ST_LEFT + "Auxiliary, Focus" + ST_RIGHT + EOL + classTwoStereotypeName, label2);
    }

    /**
     * Basic test case for {@link PackageImport} label. Prefix import should appear.
     */
    @Test
    public void testPackageImport() {
        PackageImport element = create(PackageImport.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "import" + ST_RIGHT, elementLabelProvider.getLabel(element));

        // Label is not related to source or target.
        element.setImportedPackage(createNamedElement(Package.class, "Any"));
        assertEquals(ST_LEFT + "import" + ST_RIGHT, elementLabelProvider.getLabel(element));

        element.setVisibility(VisibilityKind.PRIVATE_LITERAL);
        String accessKeyword = "access";
        assertEquals(ST_LEFT + accessKeyword + ST_RIGHT, elementLabelProvider.getLabel(element));
        element.setVisibility(VisibilityKind.PACKAGE_LITERAL);
        assertEquals(ST_LEFT + accessKeyword + ST_RIGHT, elementLabelProvider.getLabel(element));
        element.setVisibility(VisibilityKind.PROTECTED_LITERAL);
        assertEquals(ST_LEFT + accessKeyword + ST_RIGHT, elementLabelProvider.getLabel(element));
    }

    /**
     * Basic test case for {@link PackageMerge} label. Prefix merge should appear.
     */
    @Test
    public void testPackageMerge() {
        PackageMerge element = create(PackageMerge.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "merge" + ST_RIGHT, elementLabelProvider.getLabel(element));

        // Label is not related to source or target.
        element.setMergedPackage(createNamedElement(org.eclipse.uml2.uml.Package.class, "M1"));
        element.setReceivingPackage(createNamedElement(org.eclipse.uml2.uml.Package.class, "M2"));
        assertEquals(ST_LEFT + "merge" + ST_RIGHT, elementLabelProvider.getLabel(element));
    }

    @Test
    public void testConstraintLabelsWithNoSpecification() {
        Constraint constraint = create(Constraint.class);
        constraint.setName(CONST);
        StringBuilder expectedResult = new StringBuilder();
        expectedResult.append(constraint.getName());
        expectedResult.append(UMLCharacters.EOL);
        expectedResult.append(UMLCharacters.OPEN_BRACKET + "<NULL Constraint>" + UMLCharacters.CLOSE_BRACKET);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(expectedResult.toString(), elementLabelProvider.getLabel(constraint));
    }

    @Test
    public void testConstraintLabelsWithEmptyOpaqueExpressionSpec() {
        Constraint constraint = create(Constraint.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        OpaqueExpression opaqueExp = create(OpaqueExpression.class);
        constraint.setSpecification(opaqueExp);
        constraint.setName(CONST);
        StringBuilder expectedResult = new StringBuilder();
        expectedResult.append(constraint.getName());
        expectedResult.append(UMLCharacters.EOL);
        expectedResult.append(UMLCharacters.OPEN_BRACKET + UMLCharacters.OPEN_BRACKET + "NATURAL"
                + UMLCharacters.CLOSE_BRACKET + SPACE + UMLCharacters.CLOSE_BRACKET);

        assertEquals(expectedResult.toString(), elementLabelProvider.getLabel(constraint));
    }

    @Test
    public void testConstraintLabelsWithOpaqueExpressionSpec() {
        Constraint constraint = create(Constraint.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        OpaqueExpression opaqueExp = create(OpaqueExpression.class);
        opaqueExp.getLanguages().add("OCL");
        opaqueExp.getBodies().add("true");
        constraint.setSpecification(opaqueExp);
        constraint.setName(CONST);
        StringBuilder expectedResult = new StringBuilder();
        expectedResult.append(constraint.getName());
        expectedResult.append(UMLCharacters.EOL);
        expectedResult.append(UMLCharacters.OPEN_BRACKET + UMLCharacters.OPEN_BRACKET + opaqueExp.getLanguages().get(0)
                + UMLCharacters.CLOSE_BRACKET + SPACE + opaqueExp.getBodies().get(0) + UMLCharacters.CLOSE_BRACKET);

        assertEquals(expectedResult.toString(), elementLabelProvider.getLabel(constraint));
    }

    /**
     * Basic test case for {@link Collaboration} label. Prefix
     * \u00ABCollaboration\u00BB should appear.
     */
    @Test
    public void testCollaborationLabels() {
        Collaboration collaboration = create(Collaboration.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "collaboration" + ST_RIGHT, elementLabelProvider.getLabel(collaboration));
        collaboration.setName(C1_NAME);
        assertEquals(ST_LEFT + "collaboration" + ST_RIGHT + EOL + C1_NAME,
                elementLabelProvider.getLabel(collaboration));
    }

    /**
     * Basic test case for {@link Activity} label. Prefix \u00ABActivity\u00BB
     * should appear.
     */
    @Test
    public void testActivityLabels() {
        Activity activity = create(Activity.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "activity" + ST_RIGHT, elementLabelProvider.getLabel(activity));
        activity.setName("a1");
        assertEquals(ST_LEFT + "activity" + ST_RIGHT + EOL + "a1", elementLabelProvider.getLabel(activity));
    }

    @Test
    public void testFunctionBehaviorLabels() {
        FunctionBehavior functionBehavior = create(FunctionBehavior.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "functionBehavior" + ST_RIGHT, elementLabelProvider.getLabel(functionBehavior));
        functionBehavior.setName("fb1");
        assertEquals(ST_LEFT + "functionBehavior" + ST_RIGHT + EOL + "fb1",
                elementLabelProvider.getLabel(functionBehavior));
    }

    @Test
    public void testOpaqueBehaviorLabels() {
        OpaqueBehavior opaqueBehavior = create(OpaqueBehavior.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "opaqueBehavior" + ST_RIGHT, elementLabelProvider.getLabel(opaqueBehavior));
        opaqueBehavior.setName("ob1");
        assertEquals(ST_LEFT + "opaqueBehavior" + ST_RIGHT + EOL + "ob1",
                elementLabelProvider.getLabel(opaqueBehavior));
    }

    @Test
    public void testInteractionLabels() {
        Interaction interaction = create(Interaction.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "interaction" + ST_RIGHT, elementLabelProvider.getLabel(interaction));
        interaction.setName("i1");
        assertEquals(ST_LEFT + "interaction" + ST_RIGHT + EOL + "i1", elementLabelProvider.getLabel(interaction));
    }

    @Test
    public void testStateMachineLabels() {
        StateMachine stateMachine = create(StateMachine.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "stateMachine" + ST_RIGHT, elementLabelProvider.getLabel(stateMachine));
        stateMachine.setName("sm1");
        assertEquals(ST_LEFT + "stateMachine" + ST_RIGHT + EOL + "sm1", elementLabelProvider.getLabel(stateMachine));
    }

    @Test
    public void testProtocolStateMachineLabels() {
        ProtocolStateMachine protocolStateMachine = create(ProtocolStateMachine.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "protocol" + ST_RIGHT, elementLabelProvider.getLabel(protocolStateMachine));
        protocolStateMachine.setName("psm1");
        assertEquals(ST_LEFT + "protocol" + ST_RIGHT + EOL + "psm1",
                elementLabelProvider.getLabel(protocolStateMachine));
    }

    @Test
    public void testInformationFlowLabels() {
        InformationFlow informationFlow = create(InformationFlow.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + FLOW + ST_RIGHT, elementLabelProvider.getLabel(informationFlow));
        informationFlow.setName(IF1);
        assertEquals(ST_LEFT + FLOW + ST_RIGHT + EOL + IF1, elementLabelProvider.getLabel(informationFlow));

        // Test that conveyeds Elements name are displayed
        Class class1 = create(Class.class);
        class1.setName("Class1");
        Class class2 = create(Class.class);
        class2.setName("Class2");
        informationFlow.getConveyeds().addAll(List.of(class1, class2));
        assertEquals(ST_LEFT + FLOW + ST_RIGHT + EOL + "Class1, Class2" + EOL + IF1,
                elementLabelProvider.getLabel(informationFlow));

        // Test that conveyeds Elements name are displayed, even if the InformationFlow
        // has no name.

        InformationFlow informationFlow2 = create(InformationFlow.class);
        informationFlow2.getConveyeds().addAll(List.of(class1, class2));
        assertEquals(ST_LEFT + FLOW + ST_RIGHT + UMLCharacters.EOL + "Class1, Class2",
                elementLabelProvider.getLabel(informationFlow2));
    }

    @Test
    public void testComponentLabels() {
        Component element = create(Component.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "component" + ST_RIGHT, elementLabelProvider.getLabel(element));
        element.setName("comp");
        assertEquals(ST_LEFT + "component" + ST_RIGHT + EOL + "comp", elementLabelProvider.getLabel(element));
    }

    /**
     * Basic test case for {@link Region} label that should be empty.
     */
    @Test
    public void testRegionLabels() {
        Region region = create(Region.class);
        region.setName("Dummy");

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals("", elementLabelProvider.getLabel(region));
    }

    @Test
    public void testIncludeLabels() {
        Include element = create(Include.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "include" + ST_RIGHT, elementLabelProvider.getLabel(element));
        element.setName("incl");
        assertEquals(ST_LEFT + "include" + ST_RIGHT + EOL + "incl", elementLabelProvider.getLabel(element));
    }

    @Test
    public void testInterfaceLabels() {
        Interface element = create(Interface.class);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals(ST_LEFT + "interface" + ST_RIGHT, elementLabelProvider.getLabel(element));
        element.setName("inter");
        assertEquals(ST_LEFT + "interface" + ST_RIGHT + EOL + "inter", elementLabelProvider.getLabel(element));
    }

    @Test
    public void testOperationLabels() {
        Operation operation = create(Operation.class);
        Parameter p1 = create(Parameter.class);
        Parameter p2 = create(Parameter.class);

        operation.setName("op");
        p1.setName("param1");
        p2.setName("param2");
        operation.getOwnedParameters().add(p1);
        operation.getOwnedParameters().add(p2);

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        assertEquals("+ op (param1, param2)", elementLabelProvider.getLabel(operation));
        operation.setName("Custom Operation");
        p2.setName("p2");
        assertEquals("+ Custom Operation (param1, p2)", elementLabelProvider.getLabel(operation));
    }

    @Test
    public void testOnParameter() {

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        Parameter parameter = create(Parameter.class);
        parameter.setName("param1"); //$NON-NLS-1$

        assertEquals(" in param1: <Undefined>", elementLabelProvider.getLabel(parameter)); //$NON-NLS-1$

        var type = create(Class.class);
        type.setName("Class1"); //$NON-NLS-1$
        parameter.setType(type);

        assertEquals(" in param1: Class1", elementLabelProvider.getLabel(parameter)); //$NON-NLS-1$

        LiteralInteger literalInteger = create(LiteralInteger.class);
        literalInteger.setName("li1");
        literalInteger.setValue(2);
        parameter.setDefaultValue(literalInteger);

        assertEquals(" in param1: Class1 = 2", elementLabelProvider.getLabel(parameter)); //$NON-NLS-1$
    }

    @Test
    public void testOnCollaborationUse() {

        ElementLabelProvider elementLabelProvider = new ElementLabelProvider(null, new KeywordLabelProvider(),
                new DefaultNamedElementNameProvider());
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        collaborationUse.setName("cu1"); //$NON-NLS-1$

        assertEquals(" + cu1: <Undefined>", elementLabelProvider.getLabel(collaborationUse)); //$NON-NLS-1$

        Collaboration collaboration = create(Collaboration.class);
        collaboration.setName(C1_NAME); // $NON-NLS-1$
        collaborationUse.setType(collaboration);

        assertEquals(" + cu1: c1", elementLabelProvider.getLabel(collaborationUse)); //$NON-NLS-1$
    }
}
