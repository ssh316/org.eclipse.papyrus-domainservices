/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElementDomainBasedEdgeReconnectionSourceCheckerTest extends AbstractUMLTest {

    private ElementDomainBasedEdgeReconnectionSourceChecker elementDomainBasedEdgeReconnectionSourceChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        elementDomainBasedEdgeReconnectionSourceChecker = new ElementDomainBasedEdgeReconnectionSourceChecker(
                e -> true);
    }

    @Test
    public void testCanReconnectAssociationSource() {
        Model model = create(Model.class);
        UseCase sourceUseCase = createIn(UseCase.class, model);
        UseCase newSourceUseCase = createIn(UseCase.class, model);
        Association association = createIn(Association.class, model);

        CheckStatus status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association, sourceUseCase,
                newSourceUseCase);
        assertTrue(status.isValid());

        Comment newSourceComment = createIn(Comment.class, model);
        status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association, sourceUseCase,
                newSourceComment);
        assertFalse(status.isValid());
    }

    /**
     * Default source reconnection for PackageMerge.
     */
    @Test
    public void testPackageMerge() {

        Package packSource = create(Package.class);
        Package packSource2 = create(Package.class);
        Package packTarget = create(Package.class);

        PackageMerge merge = createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(merge, packSource,
                packSource2);
        assertTrue(status.isValid());
    }

    /**
     * Default source reconnection for PackageImport.
     */
    @Test
    public void testPackageImport() {

        Package packSource = create(Package.class);
        Package packSource2 = create(Package.class);
        Package packTarget = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packImport, packSource,
                packSource2);
        assertTrue(status.isValid());
    }

    /**
     * Test reconnect source with null source => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNullSource() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1,
                null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect source with no namedElement => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNoNamedElement() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Comment comment = create(Comment.class);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1,
                comment);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect source with a named element => reconnection is authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNamedElement() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Class c3 = create(Class.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1, c3);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link PackageMerge} source on a {@link Package} and on an
     * {@link Actor}.
     */
    @Test
    public void testCanReconnectPackageMergeSource() {
        // create semantic elements
        PackageMerge packageMerge = create(PackageMerge.class);
        Package source = create(Package.class);
        Package target = create(Package.class);
        packageMerge.setReceivingPackage(source);
        packageMerge.setMergedPackage(target);
        Package newSource = create(Package.class);
        Actor errorSource = create(Actor.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageMerge, source,
                newSource);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on non Package
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageMerge, source,
                errorSource);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link PackageMerge} source on a non editable {@link Package}
     * source.
     */
    @Test
    public void testCanReconnectPackageMergeSourceNoEditable() {
        // create semantic elements
        PackageMerge packageMerge = create(PackageMerge.class);
        Package source = create(Package.class);
        Package target = create(Package.class);
        packageMerge.setReceivingPackage(source);
        packageMerge.setMergedPackage(target);
        Package newSource = create(Package.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false)
                .canReconnect(packageMerge, source, newSource);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link PackageImport} source on a {@link Package} and on
     * {@link Comment}.
     */
    @Test
    public void testCanReconnectPackageImportSource() {
        // create semantic elements
        PackageImport packageImport = create(PackageImport.class);
        Package source = create(Package.class);
        Package target = create(Package.class);
        packageImport.setImportingNamespace(source);
        packageImport.setImportedPackage(target);
        Package newSource = create(Package.class);
        Comment errorSource = create(Comment.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageImport,
                source, newSource);
        assertTrue(canCreateStatus.isValid());

        // can reconnect on NameSpace
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageImport, source,
                errorSource);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link PackageImport} source on a non editable {@link Package}
     * source.
     */
    @Test
    public void testCanReconnectPackageImportSourceOnNoEditablePackage() {
        // create semantic elements
        PackageImport packageImport = create(PackageImport.class);
        Package source = create(Package.class);
        Package target = create(Package.class);
        packageImport.setImportingNamespace(source);
        packageImport.setImportedPackage(target);
        Package newSource = create(Package.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false)
                .canReconnect(packageImport, source, newSource);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Generalization} source on a {@link Classifier}, on its
     * target and on a {@link Package}.
     */
    @Test
    public void testCanReconnectGeneralizationSource() {
        // create semantic elements
        Generalization generalization = create(Generalization.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newSource = create(UseCase.class);
        Package errorSource = create(Package.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization,
                source, newSource);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on its target
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization, source, target);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Classifier
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization, source,
                errorSource);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Generalization} source on a non editable
     * {@link Classifier} source.
     */
    @Test
    public void testCanReconnectGeneralizationSourceOnNoEditableClassifier() {
        // create semantic elements
        Generalization generalization = create(Generalization.class);
        Actor source = create(Actor.class);
        UseCase target = create(UseCase.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newSource = create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false)
                .canReconnect(generalization, source, newSource);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Substitution} source on a {@link Classifier}, on its
     * target and on a {@link Package}.
     */
    @Test
    public void testCanReconnectSubstitutionSource() {
        // create semantic elements
        Substitution substitution = create(Substitution.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        substitution.setSubstitutingClassifier(source);
        substitution.setContract(target);
        UseCase newSource = create(UseCase.class);
        Package errorSource = create(Package.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution, source,
                newSource);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on its target
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution, source, target);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Classifier
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution, source,
                errorSource);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Substitution} source on a non editable
     * {@link Classifier} source.
     */
    @Test
    public void testCanReconnectSubstitutionSourceOnNoEditableClassifier() {
        // create semantic elements
        Substitution substitution = create(Substitution.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        substitution.setContract(source);
        substitution.setSubstitutingClassifier(target);
        UseCase newSource = create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false)
                .canReconnect(substitution, source, newSource);

        assertFalse(canCreateStatus.isValid());
    }

    @Test
    public void testCanReconnectTransitionSource() {
        State state1 = create(State.class);
        Pseudostate source1 = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate source2 = createIn(Pseudostate.class, region);
        Pseudostate target = createIn(Pseudostate.class, region);

        Transition transition = createIn(Transition.class, region);
        transition.setSource(source1);
        transition.setTarget(target);

        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(transition, source1,
                source2);
        assertTrue(canCreateStatus.isValid());

        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(transition, source1, region);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Usage} source on a {@link NamedElement}, on its target
     * and on a {@link Comment}.
     */
    @Test
    public void testCanReconnectUsageSource() {
        // create semantic elements
        Usage usage = create(Usage.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        usage.getClients().add(source);
        usage.getSuppliers().add(target);
        UseCase newSource = create(UseCase.class);
        Comment errorSource = create(Comment.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(usage, source,
                newSource);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on non NamedElement
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(usage, source, errorSource);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Include} with a {@link UseCase}.
     */
    @Test
    public void testCanReconnectIncludeSourceWithUseCase() {
        // create semantic elements
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newSource = create(UseCase.class);
        Actor errorSource = create(Actor.class);

        // check reconnection is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source,
                newSource);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on target
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source, target);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Use Case
        canCreateStatus = elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source, errorSource);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect source with a non editable {@link Include} .
     */
    @Test
    public void testCanReconnectIncludeSourceNoEditable() {
        // create semantic elements
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newSource = create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false)
                .canReconnect(include, source, newSource);

        assertFalse(canCreateStatus.isValid());
    }
}