/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

public class ElementDomainBasedEdgeReconnectSourceBehaviorProviderTest extends AbstractUMLTest {

    /**
     * Default source reconnection for PackageMerge.
     */
    @Test
    public void testPackageMergeSourceReconnect() {

        Package packSource = create(Package.class);
        Package packSource2 = create(Package.class);
        Package packTarget = create(Package.class);

        PackageMerge merge = createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(merge, packSource, packSource2);
        assertTrue(status.isValid());
        assertEquals(packSource2, merge.getReceivingPackage());
        assertEquals(packSource2, merge.eContainer());
    }

    /**
     * Default source reconnection for PackageMerge.
     */
    @Test
    public void testPackageImportSourceReconnect() {

        Package packSource = create(Package.class);
        Package packSource2 = create(Package.class);
        Package packTarget = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(packImport, packSource, packSource2);
        assertTrue(status.isValid());
        assertEquals(packSource2, packImport.getImportingNamespace());
        assertEquals(packSource2, packImport.eContainer());
    }

    /**
     * Test reconnect source with null dependency or null old source or null new
     * source.
     */
    @Test
    public void testDependencySourceReconnectWithNullArguments() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(null, c1, c2);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(dependency, c1, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(dependency, null, c1);

        assertFalse(status.isValid());

    }

    /**
     * Test reconnect source of dependency on an other source in the same package.
     * Expected result : source changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencySourceReconnectInSamePackage() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        org.eclipse.uml2.uml.Class c3 = createIn(org.eclipse.uml2.uml.Class.class, p1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(dependency, c1, c3);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getClients().size());
        assertEquals(c3, dependency.getClients().get(0));
    }

    /**
     * Test reconnect source of dependency on an other source in a different
     * package. Expected result : source changed and the dependency changes of owner
     * package
     */
    @Test
    public void testDependencySourceReconnectInDifferentPackage() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        org.eclipse.uml2.uml.Package p2 = create(org.eclipse.uml2.uml.Package.class);
        org.eclipse.uml2.uml.Class c3 = createIn(org.eclipse.uml2.uml.Class.class, p2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(dependency, c1, c3);

        assertTrue(status.isValid());
        assertEquals(p2, dependency.getOwner());
        assertEquals(1, dependency.getClients().size());
        assertEquals(c3, dependency.getClients().get(0));
    }

    @Test
    public void testTransitionSourceReconnect() {
        State state1 = create(State.class);
        Pseudostate source1 = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate source2 = createIn(Pseudostate.class, region);
        Pseudostate target = createIn(Pseudostate.class, region);

        Transition transition = createIn(Transition.class, region);
        transition.setSource(source1);
        transition.setTarget(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(transition, source1, source2);

        assertTrue(status.isValid());
        assertEquals(region, transition.getOwner());
        assertEquals(source2, transition.getSource());
        assertEquals(target, transition.getTarget());
    }

    /**
     * Test reconnect source of association on an other source in a different
     * package.
     */
    @Test
    public void testAssociationSourceReconnect() {
        Property sourceProp = create(Property.class);
        Actor oldSource = create(Actor.class);
        sourceProp.setType((Classifier) oldSource);

        Property targetProp = create(Property.class);
        Actor target = create(Actor.class);
        targetProp.setType((Classifier) target);

        Actor newSource = create(Actor.class);

        Association association = create(Association.class);
        association.getMemberEnds().add(targetProp);
        association.getMemberEnds().add(sourceProp);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(association, oldSource, newSource);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals((Classifier) newSource, association.getMemberEnds().get(1).getType());
        assertEquals((Classifier) target, association.getMemberEnds().get(0).getType());
    }

    @Test
    public void testGeneralizationSourceReconnect() {
        UseCase source = create(UseCase.class);
        UseCase source2 = create(UseCase.class);

        Generalization generalization = create(Generalization.class);
        generalization.setSpecific(source);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(generalization, source, source2);

        assertTrue(status.isValid());
        assertEquals(source2, generalization.getSpecific());
    }

    /**
     * Default source reconnection for {@link Include}.
     */
    @Test
    public void testIncludeSourceReconnect() {

        UseCase source = create(UseCase.class);
        UseCase source2 = create(UseCase.class);

        Include include = create(Include.class);
        include.setIncludingCase(source);

        CheckStatus status = new ElementDomainBasedEdgeReconnectSourceBehaviorProvider(IEditableChecker.TRUE)
                .reconnectSource(include, source, source2);

        assertTrue(status.isValid());
        assertEquals(source2, include.getIncludingCase());
    }
}
