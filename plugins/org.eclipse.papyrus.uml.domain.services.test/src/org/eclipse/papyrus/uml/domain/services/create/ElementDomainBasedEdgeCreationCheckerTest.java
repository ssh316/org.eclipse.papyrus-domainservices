/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.Vertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * This class tests the checking of DomainBasedEdge creation.
 * 
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy MALLET</a>
 *
 */
public class ElementDomainBasedEdgeCreationCheckerTest extends AbstractUMLTest {

    private static final String REALIZATION = "Realization";

    private static final String OWNED_ATTRIBUTE = "ownedAttribute";

    private static final String CONNECTOR = "Connector";

    private ElementDomainBasedEdgeCreationChecker elementDomainBasedEdgeCreationChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        elementDomainBasedEdgeCreationChecker = new ElementDomainBasedEdgeCreationChecker();
    }

    /**
     * Check that creation is unauthorized when we try to create a port view to its
     * self.
     */
    @Test
    public void testCanCreateConnectorFromPortOnItself() {
        // create semantic elements
        Class clazz = create(Class.class);
        Port port = createIn(Port.class, clazz, OWNED_ATTRIBUTE);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPort = classNode.addChildren(port);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeCreationChecker.canCreate(port, port, CONNECTOR, null,
                new MockedViewQuerier(), visualPort, visualPort);
        assertFalse(canCreateStatus.isValid());
        assertEquals("Cannot connect a port to itself.", canCreateStatus.getMessage());
    }

    /**
     * Check that creation is unauthorized when we try to create a connector between
     * a port view and a property view with the port as border node.
     */
    @Test
    public void testCanCreateConnectorFromPortOnItsPropertyContainerNode() {
        // create semantic elements
        Class type1 = create(Class.class);
        Property property = create(Property.class);
        property.setType(type1);
        Port port = createIn(Port.class, type1, OWNED_ATTRIBUTE);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode propertyNode = visualTree.addChildren(property);
        VisualNode visualPort = propertyNode.addBorderNode(port);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeCreationChecker.canCreate(port, property, CONNECTOR, null,
                new MockedViewQuerier(), visualPort, propertyNode);
        assertFalse(canCreateStatus.isValid());
        assertEquals("Cannot create a connector from a view representing a Part to its own Port (or the opposite).",
                canCreateStatus.getMessage());
    }

    /**
     * Check that creation is unauthorized when we try to create a connector between
     * a property view contained by an other one.
     */
    @Test
    public void testCanCreateConnectorFromPropertyContainedInAnotherOne() {
        // create semantic elements
        Class type1 = create(Class.class);
        Class clazz = create(Class.class);
        Property property1 = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        property1.setType(type1);
        Property property2 = createIn(Property.class, type1, OWNED_ATTRIBUTE);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualProperty1 = classNode.addChildren(property1);
        VisualNode visualProperty2 = visualProperty1.addChildren(property2);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeCreationChecker.canCreate(property1, property2, CONNECTOR,
                null, new MockedViewQuerier(), visualProperty1, visualProperty2);
        assertFalse(canCreateStatus.isValid());
        assertEquals(
                "Cannot connect a Part to one of its (possibly indirect) containment, must connect to one of its Port.",
                canCreateStatus.getMessage());
    }

    /**
     * Check that creation is authorized when we try to create a connector between
     * two different properties not contained in each other.
     */
    @Test
    public void testCanCreateConnectorFrom2DifferentProperty() {
        // create semantic elements
        Class clazz = create(Class.class);
        Property property1 = createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property property2 = createIn(Property.class, clazz, OWNED_ATTRIBUTE);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualProperty1 = classNode.addChildren(property1);
        VisualNode visualProperty2 = classNode.addChildren(property2);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeCreationChecker.canCreate(property1, property2, CONNECTOR,
                null, new MockedViewQuerier(), visualProperty1, visualProperty2);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Check that creation is authorized when we try to create a realization between
     * two class, i.e. when no specific condition is necessary.
     */
    @Test
    public void testCanCreateConnectorFrom2DifferentProperty2() {
        // create semantic elements
        Model model = create(Model.class);
        Class clazz1 = create(Class.class);
        Class clazz2 = create(Class.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode rootNode = visualTree.addChildren(model);
        VisualNode visualClass1 = rootNode.addChildren(clazz1);
        VisualNode visualClass2 = rootNode.addChildren(clazz2);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeCreationChecker.canCreate(clazz1, clazz2, REALIZATION, null,
                new MockedViewQuerier(), visualClass1, visualClass2);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Check the creation {@link Generalization} between classifier.
     */
    @Test
    public void testCanCreateGeneralizationOnClassifiers() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);

        // Between 2 Classifier => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c2, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());

        // Source and target should be different
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c1, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Generalization} between non classifier.
     */
    @Test
    public void testCanCreateGeneralizationOnNonClassifiers() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);

        // Source or target is not a Classifer
        Comment comment = create(Comment.class);
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, comment, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, c2, UMLPackage.eINSTANCE.getGeneralization().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Substitution} between {@link Classifier}.
     */
    @Test
    public void testCanCreateSubstitutionOnClassifiers() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);

        // Between 2 Classifier => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c2, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null).isValid());

        // Source and target should be different
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c1, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation {@link Substitution} between non {@link Classifier} source
     * and/or target.
     */
    @Test
    public void testCanCreateSubstitutionOnNonClassifiers() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);

        // Source or target is not a Classifer
        Comment comment = create(Comment.class);
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, comment, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, c2, UMLPackage.eINSTANCE.getSubstitution().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Manifestation} between {@link NamedElement} and
     * {@link PackageableElement}.
     */
    @Test
    public void testCanCreateManifestationBetweenNamedElementAndPackageableElement() {
        Class c1 = create(Class.class);
        Usage u1 = create(Usage.class);

        // Between one {@link NamedElement} and one {@link PackageableElement} => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, u1, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());

        // Source and target should be different
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c1, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Manifestation} between non {@link NamedElement} and
     * non {@link PackageableElement}.
     */
    @Test
    public void testCanCreateManifestationBetweenNonNamedElementAndNonPackageableElement() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);

        // Source or target is not a NamedElement or not Packageable element.
        Comment comment = create(Comment.class);
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, comment, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, c2, UMLPackage.eINSTANCE.getManifestation().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Manifestation} between non {@link NamedElement} and
     * non {@link PackageableElement}.
     */
    @Test
    public void testCanCreateTransition() {
        Vertex v1 = create(State.class);
        Vertex v2 = create(State.class);

        assertTrue(
                elementDomainBasedEdgeCreationChecker.canCreate(v1, v2, UMLPackage.eINSTANCE.getTransition().getName(),
                        UMLPackage.eINSTANCE.getRegion_Transition().getName(), null, null, null).isValid());

        // source is not a vertex
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(create(Class.class), v2, UMLPackage.eINSTANCE.getTransition().getName(),
                        UMLPackage.eINSTANCE.getRegion_Transition().getName(), null, null, null)
                .isValid());
        // target is not a vertex
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(v1, create(Class.class), UMLPackage.eINSTANCE.getTransition().getName(),
                        UMLPackage.eINSTANCE.getRegion_Transition().getName(), null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Association} between {@link Classifier} and
     * {@link Classifier}.
     */
    @Test
    public void testCanCreateAssociatinOnClassifiers() {
        Actor actor1 = create(Actor.class);
        Actor actor2 = create(Actor.class);

        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(actor1, actor2, UMLPackage.eINSTANCE.getAssociation().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Association} between {@link Classifier} and non
     * {@link Classifier}.
     */
    @Test
    public void testCanCreateAssociatinBetweenClassifiersAndNonClassifiers() {
        Actor actor = create(Actor.class);
        Comment comment = create(Comment.class);

        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(actor, comment, UMLPackage.eINSTANCE.getAssociation().getName(), null, null, null, null)
                .isValid());

        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getAssociation().getName(), null, null, null, null)
                .isValid());

        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, actor, UMLPackage.eINSTANCE.getAssociation().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Include} between non {@link UseCase} element.
     */
    @Test
    public void testCanCreateIncludeBetweenNonUseCase() {
        Actor actor = create(Actor.class);
        Package pack = create(Package.class);

        // Between non {@link UseCase} => KO
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(actor, pack, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null).isValid());

        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(pack, actor, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation {@link PackageMerge} between {@link Package}.
     */
    @Test
    public void testCanCreatePackageMerge() {
        Package pack1 = create(Package.class);
        Package pack2 = create(Package.class);

        // Between {@link Package} => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(pack1, pack2, UMLPackage.eINSTANCE.getPackageMerge().getName(), null, null, null, null)
                .isValid());

        // Source and target can be different
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(pack1, pack2, UMLPackage.eINSTANCE.getPackageMerge().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link PackageMerge} with a non {@link Package} element.
     */
    @Test
    public void testCanCreatePackageMergeWithNonPackageElement() {
        Package pack = create(Package.class);
        Actor actor = create(Actor.class);

        // Between {@link Package} and non {@link Package} => KO
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(pack, actor, UMLPackage.eINSTANCE.getPackageMerge().getName(), null, null, null, null)
                .isValid());

        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(actor, pack, UMLPackage.eINSTANCE.getPackageMerge().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link PackageImport} between {@link Package}.
     */
    @Test
    public void testCanCreatePackageImport() {
        Package pack1 = create(Package.class);
        Package pack2 = create(Package.class);

        // Between {@link Package} => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(pack1, pack2, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());

        // Source and target can be different
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(pack1, pack2, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link PackageMerge} between a {@link Package} and a
     * {@link NameSpace} element.
     */
    @Test
    public void testCanCreatePackageImportWithNonPackageElement() {
        Package pack = create(Package.class);
        Actor actor = create(Actor.class);
        Comment comment = create(Comment.class);

        // Between {@link Package} and non {@link Package} => KO
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(pack, actor, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());

        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(actor, pack, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());

        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, pack, UMLPackage.eINSTANCE.getPackageImport().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Usage} between {@link NamedElement} and non
     * {@link NamedElement}.
     */
    @Test
    public void testCanCreateDependencyWithNonNamedElement() {
        Actor actor1 = create(Actor.class);
        Interaction interaction2 = create(Interaction.class);

        // Source or target is not a NamedElement element.
        Comment comment = create(Comment.class);
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(actor1, comment, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker.canCreate(comment, interaction2,
                UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null).isValid());
    }

    /**
     * Check the creation {@link Manifestation} between {@link NamedElement} and
     * {@link PackageableElement}.
     */
    @Test
    public void testCanCreateDependency() {
        Actor actor = create(Actor.class);
        Interaction interaction = create(Interaction.class);

        // Between {@link NamedElement} => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(actor, interaction, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());

        // Source and target can be different
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(actor, actor, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());

        // Source or target is not a NamedElement
        Comment comment = create(Comment.class);
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(actor, comment, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, actor, UMLPackage.eINSTANCE.getDependency().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Include} between {@link UseCase}.
     */
    @Test
    public void testCanCreateInclude() {
        UseCase useCase1 = create(UseCase.class);
        UseCase useCase2 = create(UseCase.class);

        // Between {@link UseCase} => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, useCase2, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());

        // Source and target should be different
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, useCase1, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());

        // Source or target is not a UseCase
        Comment comment = create(Comment.class);
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, comment, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, useCase2, UMLPackage.eINSTANCE.getInclude().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation of {@link InformationFlow} between {@link NamedElement}.
     */
    @Test
    public void testCanCreateInformationFlowOnNamedElements() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);

        // Between 2 Named element => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, c2, UMLPackage.eINSTANCE.getInformationFlow().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Generalization} between non {@link NamedElement}.
     */
    @Test
    public void testCanCreateInformationFlowOnNonNamedElements() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);

        // Source or target is not a {@link NamedElement}
        Comment comment = create(Comment.class);
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(c1, comment, UMLPackage.eINSTANCE.getInformationFlow().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker.canCreate(comment, comment,
                UMLPackage.eINSTANCE.getInformationFlow().getName(), null, null, null, null).isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, c2, UMLPackage.eINSTANCE.getInformationFlow().getName(), null, null, null, null)
                .isValid());
    }

    /**
     * Check the creation {@link Extend} between {@link UseCase}.
     */
    @Test
    public void testCanCreateExtend() {
        UseCase useCase1 = create(UseCase.class);
        UseCase useCase2 = create(UseCase.class);

        // Between {@link UseCase} => OK
        assertTrue(elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, useCase2, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());

        // Source and target should be different
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, useCase1, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());

        // Source or target is not a UseCase
        Comment comment = create(Comment.class);
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(useCase1, comment, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, comment, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());
        assertFalse(elementDomainBasedEdgeCreationChecker
                .canCreate(comment, useCase2, UMLPackage.eINSTANCE.getExtend().getName(), null, null, null, null)
                .isValid());
    }

}
