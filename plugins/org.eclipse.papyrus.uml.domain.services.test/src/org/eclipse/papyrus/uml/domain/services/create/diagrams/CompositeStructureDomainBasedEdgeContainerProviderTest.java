/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create.diagrams;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.edges.IDomainBasedEdgeContainerProvider;
import org.eclipse.papyrus.uml.domain.services.edges.diagrams.CompositeStructureDomainBasedEdgeContainerProvider;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLFactory;
import org.junit.jupiter.api.Test;

public class CompositeStructureDomainBasedEdgeContainerProviderTest {

    private UMLFactory fact = UMLFactory.eINSTANCE;

    @Test
    public void containerOfConnectorOnPropertyWithCommonAncestor() {

        Class commonClass = fact.createClass();

        Property p1 = fact.createProperty();
        commonClass.getOwnedAttributes().add(p1);
        Property p2 = fact.createProperty();
        commonClass.getOwnedAttributes().add(p2);

        Class p1Type = fact.createClass();
        p1.setType(p1Type);
        Port port1 = fact.createPort();
        p1Type.getOwnedAttributes().add(port1);

        Class p2Type = fact.createClass();
        p2.setType(p2Type);
        Port port2 = fact.createPort();
        p2Type.getOwnedAttributes().add(port2);

        VisualTree visualTree = new VisualTree();

        VisualNode rootNode = visualTree.addChildren(commonClass);

        VisualNode visualPort1 = rootNode.addChildren(p1).addChildren(port1);
        VisualNode visualPort2 = rootNode.addChildren(p2).addChildren(port2);

        CompositeStructureDomainBasedEdgeContainerProvider provider = new CompositeStructureDomainBasedEdgeContainerProvider(
                new IDomainBasedEdgeContainerProvider.NoOP());

        Connector connector = fact.createConnector();

        EObject container = provider.getContainer(p1, p2, connector, new MockedViewQuerier(), visualPort1, visualPort2);

        assertTrue(container == commonClass);
    }

    @Test
    public void containerOfConnectorOnPropertyWithNOCommonAncestor() {
        Class c1 = fact.createClass();
        Class c2 = fact.createClass();

        Property p1 = fact.createProperty();
        c1.getOwnedAttributes().add(p1);
        Property p2 = fact.createProperty();
        c2.getOwnedAttributes().add(p2);

        Class p1Type = fact.createClass();
        p1.setType(p1Type);
        Port port1 = fact.createPort();
        p1Type.getOwnedAttributes().add(port1);

        Class p2Type = fact.createClass();
        p2.setType(p2Type);
        Port port2 = fact.createPort();
        p2Type.getOwnedAttributes().add(port2);

        VisualTree visualTree = new VisualTree();

        VisualNode visualC1 = visualTree.addChildren(c1);
        VisualNode visualC2 = visualTree.addChildren(c2);

        VisualNode visualPort1 = visualC1.addChildren(p1).addChildren(port1);
        VisualNode visualPort2 = visualC2.addChildren(p2).addChildren(port2);

        CompositeStructureDomainBasedEdgeContainerProvider provider = new CompositeStructureDomainBasedEdgeContainerProvider(
                new IDomainBasedEdgeContainerProvider.NoOP());

        Connector connector = fact.createConnector();

        EObject container = provider.getContainer(p1, p2, connector, new MockedViewQuerier(), visualPort1, visualPort2);

        assertNull(container);
    }

}
