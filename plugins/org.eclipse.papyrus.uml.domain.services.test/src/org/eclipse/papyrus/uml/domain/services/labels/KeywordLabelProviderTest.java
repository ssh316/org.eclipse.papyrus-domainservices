/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels;

import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_LEFT;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_RIGHT;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.ProtocolStateMachine;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Signal;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.VisibilityKind;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link KeywordLabelProvider}.
 * 
 * @author Jessy MALLET
 *
 */
public class KeywordLabelProviderTest extends AbstractUMLTest {

    private KeywordLabelProvider keywordLabelProvider;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        keywordLabelProvider = new KeywordLabelProvider();
    }

    @Test
    public void testOnNull() {
        assertEquals(null, keywordLabelProvider.apply(null));
    }

    /**
     * Basic test case for {@link Abstraction} keyword. Prefix
     * \u00ABabstraction\u00BB should appear.
     */
    @Test
    public void testAbstractionKeyword() {
        Abstraction abstraction = create(Abstraction.class);
        assertEquals(ST_LEFT + "abstraction" + ST_RIGHT, keywordLabelProvider.apply(abstraction));
    }

    /**
     * Basic test case for {@link Activity} keyword. Prefix \u00ABactivity\u00BB
     * should appear.
     */
    @Test
    public void testActivityKeyword() {
        Activity activity = create(Activity.class);
        assertEquals(ST_LEFT + "activity" + ST_RIGHT, keywordLabelProvider.apply(activity));
    }

    /**
     * Basic test case for {@link Collaboration} keyword. Prefix
     * \u00ABcollaboration\u00BB should appear.
     */
    @Test
    public void testCollaborationKeyword() {
        Collaboration collaboration = create(Collaboration.class);
        assertEquals(ST_LEFT + "collaboration" + ST_RIGHT, keywordLabelProvider.apply(collaboration));
    }

    /**
     * Basic test case for {@link Component} keyword. Prefix \u00ABcomponent\u00BB
     * should appear.
     */
    @Test
    public void testComponentKeyword() {
        Component component = create(Component.class);
        assertEquals(ST_LEFT + "component" + ST_RIGHT, keywordLabelProvider.apply(component));
    }

    /**
     * Basic test case for {@link Extend} keyword. Prefix \u00ABextend\u00BB should
     * appear.
     */
    @Test
    public void testExtendKeyword() {
        Extend extend = create(Extend.class);
        assertEquals(ST_LEFT + "extend" + ST_RIGHT, keywordLabelProvider.apply(extend));
    }

    /**
     * Basic test case for {@link FunctionBehavior} keyword. Prefix
     * \u00ABfunctionBehavior\u00BB should appear.
     */
    @Test
    public void testFunctionBehaviorKeyword() {
        FunctionBehavior functionBehavior = create(FunctionBehavior.class);
        assertEquals(ST_LEFT + "functionBehavior" + ST_RIGHT, keywordLabelProvider.apply(functionBehavior));
    }

    /**
     * Basic test case for {@link Include} keyword. Prefix \u00ABinclude\u00BB
     * should appear.
     */
    @Test
    public void testIncludeKeyword() {
        Include include = create(Include.class);
        assertEquals(ST_LEFT + "include" + ST_RIGHT, keywordLabelProvider.apply(include));
    }

    /**
     * Basic test case for {@link Realization} keyword. No Prefix should appear.
     */
    @Test
    public void testRealizationKeyword() {
        Realization realization = create(Realization.class);
        assertEquals(null, keywordLabelProvider.apply(realization));
    }

    /**
     * Basic test case for {@link InformationFlow} keyword. Prefix \u00ABflow\u00BB
     * should appear.
     */
    @Test
    public void testInformationFlowKeyword() {
        InformationFlow informationFlow = create(InformationFlow.class);
        assertEquals(ST_LEFT + "flow" + ST_RIGHT, keywordLabelProvider.apply(informationFlow));
    }

    /**
     * Basic test case for {@link InformationItem} keyword. Prefix
     * \u00ABinformation\u00BB should appear.
     */
    @Test
    public void testInformationItemKeyword() {
        InformationItem informationItem = create(InformationItem.class);
        assertEquals(ST_LEFT + "information" + ST_RIGHT, keywordLabelProvider.apply(informationItem));
    }

    /**
     * Basic test case for {@link Interaction} keyword. Prefix
     * \u00ABinteraction\u00BB should appear.
     */
    @Test
    public void testInteractionKeyword() {
        Interaction interaction = create(Interaction.class);
        assertEquals(ST_LEFT + "interaction" + ST_RIGHT, keywordLabelProvider.apply(interaction));
    }

    /**
     * Basic test case for {@link Interface} keyword. Prefix \u00ABinterface\u00BB
     * should appear.
     */
    @Test
    public void testInterfaceKeyword() {
        Interface interface1 = create(Interface.class);
        assertEquals(ST_LEFT + "interface" + ST_RIGHT, keywordLabelProvider.apply(interface1));
    }

    /**
     * Basic test case for {@link Manifestation} keyword. Prefix
     * \u00ABmanifest\u00BB should appear.
     */
    @Test
    public void testManifestationKeyword() {
        Manifestation manifestation = create(Manifestation.class);
        assertEquals(ST_LEFT + "manifest" + ST_RIGHT, keywordLabelProvider.apply(manifestation));
    }

    /**
     * Basic test case for {@link OpaqueBehavior} keyword. Prefix
     * \u00ABopaqueBehavior\u00BB should appear.
     */
    @Test
    public void testOpaqueBehaviorKeyword() {
        OpaqueBehavior opaqueBehavior = create(OpaqueBehavior.class);
        assertEquals(ST_LEFT + "opaqueBehavior" + ST_RIGHT, keywordLabelProvider.apply(opaqueBehavior));
    }

    /**
     * Basic test case for {@link PackageImport} keyword. Prefix \u00ABaccess\u00BB
     * should appear or \u00ABimport\u00BB if visibility is public.
     */
    @Test
    public void testPackageImportKeyword() {
        PackageImport packageImport = create(PackageImport.class);
        assertEquals(ST_LEFT + "import" + ST_RIGHT, keywordLabelProvider.apply(packageImport));
        packageImport.setVisibility(VisibilityKind.PRIVATE_LITERAL);
        assertEquals(ST_LEFT + "access" + ST_RIGHT, keywordLabelProvider.apply(packageImport));
    }

    /**
     * Basic test case for {@link PackageMerge} keyword. Prefix \u00ABmerge\u00BB
     * should appear.
     */
    @Test
    public void testPackageMergeKeyword() {
        PackageMerge packageMerge = create(PackageMerge.class);
        assertEquals(ST_LEFT + "merge" + ST_RIGHT, keywordLabelProvider.apply(packageMerge));
    }

    /**
     * Basic test case for {@link ProtocolStateMachine} keyword. Prefix
     * \u00ABprotocol\u00BB should appear.
     */
    @Test
    public void testProtocolStateMachineKeyword() {
        ProtocolStateMachine protocol = create(ProtocolStateMachine.class);
        assertEquals(ST_LEFT + "protocol" + ST_RIGHT, keywordLabelProvider.apply(protocol));
    }

    /**
     * Basic test case for {@link Signal} keyword. Prefix \u00ABsignal\u00BB should
     * appear.
     */
    @Test
    public void testSignalKeyword() {
        Signal signal = create(Signal.class);
        assertEquals(ST_LEFT + "signal" + ST_RIGHT, keywordLabelProvider.apply(signal));
    }

    /**
     * Basic test case for {@link StateMachine} keyword. Prefix
     * \u00ABstateMachine\u00BB should appear.
     */
    @Test
    public void testStateMachineKeyword() {
        StateMachine stateMachine = create(StateMachine.class);
        assertEquals(ST_LEFT + "stateMachine" + ST_RIGHT, keywordLabelProvider.apply(stateMachine));
    }

    /**
     * Basic test case for {@link Substitution} keyword. Prefix
     * \u00ABsubstitute\u00BB should appear.
     */
    @Test
    public void testSubstitutionKeyword() {
        Substitution substitution = create(Substitution.class);
        assertEquals(ST_LEFT + "substitute" + ST_RIGHT, keywordLabelProvider.apply(substitution));
    }

    /**
     * Basic test case for {@link Usage} keyword. Prefix \u00ABuse\u00BB should
     * appear.
     */
    @Test
    public void testUsageKeyword() {
        Usage usage = create(Usage.class);
        assertEquals(ST_LEFT + "use" + ST_RIGHT, keywordLabelProvider.apply(usage));
    }
}
