/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.modify;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Usage;
import org.junit.jupiter.api.Test;

public class ElementFeatureModifierTest extends AbstractUMLTest {

    private static final String AGGREGATION_REF = "aggregation";
    private static final String NAME_REF = "name";
    private static final String TYPE_REF = "type";
    private static final String CLASS1 = "class1";
    private static final String ANNOTATED_ELEMENT = UMLPackage.eINSTANCE.getComment_AnnotatedElement().getName();

    /**
     * Test basic use case of setting an EAttribute.
     */
    @Test
    public void basicSetUnaryAttribute() {
        Class clazz = create(Class.class);
        Status status = buildElementFeatureModifier().setValue(clazz, NAME_REF, CLASS1);
        assertEquals(State.DONE, status.getState());
        assertEquals(CLASS1, clazz.getName());
    }

    /**
     * Check that a feature can be modified on a element that can't be edited.
     */
    @Test
    public void testUnEditableElement() {
        Class clazz = create(Class.class);
        Status status = new ElementFeatureModifier(getCrossRef(), e -> false).setValue(clazz, NAME_REF, CLASS1);
        assertEquals(State.FAILED, status.getState());
        assertNull(clazz.getName());
    }

    /**
     * Test basic use case of setting an unary EReference.
     */
    @Test
    public void basicSetUnaryReference() {
        Property prop = create(Property.class);
        Class type = create(Class.class);

        Status status = buildElementFeatureModifier().setValue(prop, TYPE_REF, type);
        assertEquals(State.DONE, status.getState());
        assertEquals(type, prop.getType());
    }

    /**
     * Test basic use case of adding a EObject a "many" EReference.
     */
    @Test
    public void basicAddReference() {
        Comment comment = create(Comment.class);
        Class type = create(Class.class);

        Status status = buildElementFeatureModifier().addValue(comment, ANNOTATED_ELEMENT, type);
        assertEquals(State.DONE, status.getState());
        assertTrue(comment.getAnnotatedElements().contains(type));
    }

    /**
     * Test basic use case of setting a EObject in a "many" EReference => The only
     * element left is set object.
     */
    @Test
    public void basicSetOnManyReference() {
        Comment comment = create(Comment.class);
        Class type = create(Class.class);
        Class type2 = create(Class.class);
        comment.getAnnotatedElements().add(type2);

        Status status = buildElementFeatureModifier().setValue(comment, ANNOTATED_ELEMENT, type);
        assertEquals(State.DONE, status.getState());
        assertEquals(List.of(type), comment.getAnnotatedElements());
    }

    /**
     * Test basic use case of removing a EObject a "many" EReference.
     */
    @Test
    public void basicRemoveReference() {
        Comment comment = create(Comment.class);
        Class type = create(Class.class);
        comment.getAnnotatedElements().add(type);

        Status status = buildElementFeatureModifier().removeValue(comment, ANNOTATED_ELEMENT, type);
        assertEquals(State.DONE, status.getState());
        assertFalse(comment.getAnnotatedElements().contains(type));
    }

    /**
     * Test setting a derived feature => Failure.
     */
    @Test
    public void basicSetDeriveFeature() {
        Class clazz = create(Class.class);
        Class superClazz = create(Class.class);
        Status status = buildElementFeatureModifier().setValue(clazz, "superClass", superClazz);
        assertEquals(State.FAILED, status.getState());
        assertFalse(clazz.getSuperClasses().contains(superClazz));
    }

    /**
     * Test setting feature which is not existing => Failure.
     */
    @Test
    public void basicSetNonExisitingFeature() {
        Class clazz = create(Class.class);
        Class superClazz = create(Class.class);
        Status status = buildElementFeatureModifier().setValue(clazz, "superMegaClass", superClazz);
        assertEquals(State.FAILED, status.getState());
    }

    private ElementFeatureModifier buildElementFeatureModifier() {
        return new ElementFeatureModifier(getCrossRef(), getEditableChecker());
    }

    /**
     * Test unsetting property type for a member end of an Association: the
     * Association should be removed.
     */
    @Test
    public void testUnsetPropertyTypeAssociationMemberEnd() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);
        memberEnds.get(0).setType(class1);
        memberEnds.get(1).setType(class2);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, null);
        assertEquals(State.DONE, status.getState());
        assertTrue(model.getPackagedElements().isEmpty());
        assertNull(property1.getAssociation());
    }

    /**
     * Test unsetting port type for a member end of an Association: the Association
     * should not be removed.
     */
    @Test
    public void testUnsetPortTypeAssociationMemberEnd() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Association asso = create(Association.class);
        Port port1 = create(Port.class);
        Port port2 = create(Port.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(port1);
        memberEnds.add(port2);
        memberEnds.get(0).setType(class1);
        memberEnds.get(1).setType(class2);

        Status status = buildElementFeatureModifier().setValue(port1, TYPE_REF, null);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(port1, asso.getMemberEnds().get(0));
        assertNotNull(port1.getAssociation());
    }

    /**
     * Test setting property type without adding a member end to the association:
     * the Association should not be removed.
     */
    @Test
    public void testSetPropertyTypeWithoutAssociation() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);

        model.getPackagedElements().add(asso);
        property1.setType(class1);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, class2);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertNull(property1.getAssociation());
    }

    /**
     * Test setting the unary EReference "type" for a member end of an Association
     * with 2 member end. Expected result : the Association should be removed.
     */
    @Test
    public void testUnsetTypeAssociationMemberEndWithRemoveAssociation() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);

        Association asso = createIn(Association.class, model);
        Property property1 = create(Property.class);
        property1.setType(class1);
        Property property2 = create(Property.class);
        property2.setType(class1);

        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, class2);
        assertEquals(State.DONE, status.getState());
        assertTrue(model.getPackagedElements().isEmpty());
    }

    /**
     * Test setting the "aggregation" feature without setting memberEnds to the
     * association. The Association should not be removed and AggregationKind should
     * remain unchanged.
     */
    @Test
    public void testSetPropertyAggregationWithoutAssociation() {
        Model model = create(Model.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);

        model.getPackagedElements().add(asso);
        property2.setAggregation(AggregationKind.COMPOSITE_LITERAL);

        Status status = buildElementFeatureModifier().setValue(property1, AGGREGATION_REF,
                AggregationKind.COMPOSITE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, property1.getAggregation());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, property2.getAggregation());
    }

    /**
     * Test setting the "aggregation" feature with memberEnds added to the
     * association. The Association should not be deleted but others AggregationKind
     * should be updated to NONE_LITERAL.
     */
    @Test
    public void testSetPropertyAggregationWithAssociationMemberEnds() {
        Model model = create(Model.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);
        property2.setAggregation(AggregationKind.COMPOSITE_LITERAL);

        Status status = buildElementFeatureModifier().setValue(property1, AGGREGATION_REF,
                AggregationKind.COMPOSITE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, property1.getAggregation());
        assertEquals(AggregationKind.NONE_LITERAL, property2.getAggregation());

        status = buildElementFeatureModifier().setValue(property1, AGGREGATION_REF, AggregationKind.NONE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.NONE_LITERAL, property1.getAggregation());
        assertEquals(AggregationKind.NONE_LITERAL, property2.getAggregation());
    }

    /**
     * Test setting the "aggregation" feature with NONE_LITERAL value. The
     * Association should not be removed and AggregationKind should remain
     * unchanged.
     */
    @Test
    public void testSetPropertyNONEAggregationWithAssociationMemberEnds() {
        Model model = create(Model.class);
        Association asso = create(Association.class);
        Property property1 = create(Property.class);
        Property property2 = create(Property.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);
        property2.setAggregation(AggregationKind.COMPOSITE_LITERAL);

        Status status = buildElementFeatureModifier().setValue(property1, AGGREGATION_REF,
                AggregationKind.NONE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.NONE_LITERAL, property1.getAggregation());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, property2.getAggregation());
    }

    /**
     * Test setting the "aggregation" feature for a Port. The Association should not
     * be removed and AggregationKind should remain unchanged.
     */
    @Test
    public void testSetPortAggregationWithAssociationMemberEnds() {
        Model model = create(Model.class);
        Association asso = create(Association.class);
        Port port1 = create(Port.class);
        Port port2 = create(Port.class);

        model.getPackagedElements().add(asso);
        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(port1);
        memberEnds.add(port2);
        port2.setAggregation(AggregationKind.COMPOSITE_LITERAL);

        Status status = buildElementFeatureModifier().setValue(port1, AGGREGATION_REF,
                AggregationKind.COMPOSITE_LITERAL);
        assertEquals(State.DONE, status.getState());
        assertFalse(model.getPackagedElements().isEmpty());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port1.getAggregation());
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port2.getAggregation());
    }

    /**
     * Test setting the unary EReference "type" for a member end of an Association
     * with 2 member end. Expected result : the Association still exists.
     */
    @Test
    public void testUnsetTypeAssociationMemberEndWithNoRemoveAssociation() {
        Model model = create(Model.class);
        Class class1 = create(Class.class);
        Class class2 = create(Class.class);

        Association asso = createIn(Association.class, model);
        Property property1 = create(Property.class);
        property1.setType(class1);
        Property property2 = create(Property.class);
        property2.setType(class1);
        Property property3 = create(Property.class);
        property3.setType(class1);

        EList<Property> memberEnds = asso.getMemberEnds();
        memberEnds.add(property1);
        memberEnds.add(property2);
        memberEnds.add(property3);

        Status status = buildElementFeatureModifier().setValue(property1, TYPE_REF, class2);
        assertEquals(State.DONE, status.getState());
        assertEquals(1, model.getPackagedElements().size());
        assertEquals(asso, model.getPackagedElements().get(0));
    }

    /**
     * Tests changing the type of a CollaborationUse. The RoleBindings feature
     * should be cleared.
     */
    @Test
    public void testChangeTypeCollaborationUse() {
        Collaboration collaboration1 = create(Collaboration.class);
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        collaborationUse.setType(collaboration1);
        Usage usage = create(Usage.class);
        Abstraction abstraction = create(Abstraction.class);
        Dependency dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(usage);
        collaborationUse.getRoleBindings().add(abstraction);
        collaborationUse.getRoleBindings().add(dependency);

        assertFalse(collaborationUse.getRoleBindings().isEmpty());
        Collaboration c2 = create(Collaboration.class);
        Status status = buildElementFeatureModifier().setValue(collaborationUse, TYPE_REF, c2);
        assertEquals(State.DONE, status.getState());
        assertTrue(collaborationUse.getRoleBindings().isEmpty());

        // Set the same type again doesn't clear the roleBindings feature
        usage = create(Usage.class);
        abstraction = create(Abstraction.class);
        dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(usage);
        collaborationUse.getRoleBindings().add(abstraction);
        collaborationUse.getRoleBindings().add(dependency);
        status = buildElementFeatureModifier().setValue(collaborationUse, TYPE_REF, c2);
        assertEquals(State.DONE, status.getState());
        assertFalse(collaborationUse.getRoleBindings().isEmpty());
    }

    /**
     * Tests removing a roleBinding from a CollaborationUse.
     */
    @Test
    public void testRemoveRoleBindingCollaborationUse() {
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        Dependency dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(dependency);

        assertFalse(collaborationUse.getRoleBindings().isEmpty());
        Status status = buildElementFeatureModifier().removeValue(collaborationUse, "roleBinding", dependency);
        assertEquals(State.DONE, status.getState());
        assertTrue(collaborationUse.getRoleBindings().isEmpty());
    }

    /**
     * Tests setting null value to the type of a CollaborationUse. The RoleBindings
     * feature should be cleared.
     */
    @Test
    public void testChangeTypeCollaborationUseWithNullValue() {
        Collaboration collaboration = create(Collaboration.class);
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        collaborationUse.setType(collaboration);
        Dependency dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(dependency);

        assertFalse(collaborationUse.getRoleBindings().isEmpty());
        Status status = buildElementFeatureModifier().setValue(collaborationUse, TYPE_REF, null);
        assertEquals(State.DONE, status.getState());
        assertTrue(collaborationUse.getRoleBindings().isEmpty());
    }

    /**
     * Tests setting a non-Collaboration instance to the type of a CollaborationUse.
     * It should failed without doing anything.
     */
    @Test
    public void testChangeTypeCollaborationUseWithNotCollaborationInstance() {
        Property property = create(Property.class);
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        Dependency dependency = create(Dependency.class);
        collaborationUse.getRoleBindings().add(dependency);

        assertFalse(collaborationUse.getRoleBindings().isEmpty());
        Status status = buildElementFeatureModifier().setValue(collaborationUse, TYPE_REF, property);
        assertEquals(State.FAILED, status.getState());
        assertFalse(collaborationUse.getRoleBindings().isEmpty());
    }
}
