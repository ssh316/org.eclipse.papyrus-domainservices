/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.edges;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.List;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeTargetsProvider}.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDomainBasedEdgeTargetsProviderTest extends AbstractUMLTest {

    private ElementDomainBasedEdgeTargetsProvider targetProvider = new ElementDomainBasedEdgeTargetsProvider();

    /**
     * Tests source provider on {link Dependency}.
     */
    @Test
    public void testDependency() {
        Dependency dependency = create(Dependency.class);
        assertTrue(targetProvider.getTargets(dependency).isEmpty());

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        dependency.getSuppliers().add(class1);
        assertEquals(List.of(class1), targetProvider.getTargets(dependency));
    }

    /**
     * Tests source provider on {link Generalization}.
     */
    @Test
    public void testGeneralization() {
        Generalization generalization = create(Generalization.class);
        assertTrue(targetProvider.getTargets(generalization).isEmpty());

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        generalization.setGeneral(class1);
        assertEquals(List.of(class1), targetProvider.getTargets(generalization));
    }

    /**
     * Tests source provider on {link Substitution}.
     */
    @Test
    public void testSubstitution() {
        Substitution substitution = create(Substitution.class);
        assertTrue(targetProvider.getTargets(substitution).isEmpty());

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        substitution.setContract(class1);
        assertEquals(List.of(class1), targetProvider.getTargets(substitution));
    }

    /**
     * Tests source provider on {link Manifestation}.
     */
    @Test
    public void testManifestation() {
        Manifestation manifestation = create(Manifestation.class);
        assertTrue(targetProvider.getTargets(manifestation).isEmpty());

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        manifestation.setUtilizedElement(class1);
        assertEquals(List.of(class1), targetProvider.getTargets(manifestation));
    }

    /**
     * Tests source provider on {link PackageImport}.
     */
    @Test
    public void casePackageImport() {
        PackageImport element = create(PackageImport.class);
        assertEquals(Collections.emptyList(), targetProvider.getTargets(element));

        Package target = create(org.eclipse.uml2.uml.Package.class);
        element.setImportedPackage(target);
        assertEquals(List.of(target), targetProvider.getTargets(element));
    }

    /**
     * Tests source provider on {link PackageImport}.
     */
    @Test
    public void casePackageMerge() {
        PackageMerge element = create(PackageMerge.class);
        assertEquals(Collections.emptyList(), targetProvider.getTargets(element));

        Package target = create(org.eclipse.uml2.uml.Package.class);
        element.setMergedPackage(target);
        assertEquals(List.of(target), targetProvider.getTargets(element));
    }

    @Test
    public void caseTransition() {
        Pseudostate source = create(Pseudostate.class);
        Pseudostate target = create(Pseudostate.class);

        Transition transition = create(Transition.class);
        transition.setSource(source);
        transition.setTarget(target);

        assertEquals(List.of(target), targetProvider.getTargets(transition));
    }

    @Test
    public void caseAssociation() {
        Association association = create(Association.class);
        Actor source = create(Actor.class);
        Actor target = create(Actor.class);
        new ElementDomainBasedEdgeInitializer().initialize(association, source, target, null, null, null);

        assertEquals(List.of(target), targetProvider.getTargets(association));

        // if type of source Property is null, none target is found
        association.getMemberEnds().get(0).setType(null);
        assertEquals(Collections.emptyList(), targetProvider.getTargets(association));
    }

    @Test
    public void caseInclude() {
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        new ElementDomainBasedEdgeInitializer().initialize(include, source, target, null, null, null);

        assertEquals(List.of(target), targetProvider.getTargets(include));
    }

    /**
     * Tests source provider on {link InformationFlow}.
     */
    @Test
    public void testInformationFlow() {
        InformationFlow informationFlow = create(InformationFlow.class);
        assertTrue(targetProvider.getTargets(informationFlow).isEmpty());

        Class class1 = create(org.eclipse.uml2.uml.Class.class);
        informationFlow.getInformationTargets().add(class1);
        assertEquals(List.of(class1), targetProvider.getTargets(informationFlow));
    }

    @Test
    public void caseExtend() {
        Extend extend = create(Extend.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        new ElementDomainBasedEdgeInitializer().initialize(extend, source, target, null, null, null);

        assertEquals(List.of(target), targetProvider.getTargets(extend));
    }
}
