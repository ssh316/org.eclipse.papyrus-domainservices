/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.edges;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.ExtensionPoint;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeInitializer}.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDomainBasedEdgeInitializerTest extends AbstractUMLTest {

    private static final String NATURE = "nature";

    /**
     * Basic test case for Dependency initialization.
     */
    @Test
    public void testDependency() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        Dependency dependency = create(Dependency.class);

        new ElementDomainBasedEdgeInitializer().initialize(dependency, c1, c2, null, null, null);

        assertEquals(c1, dependency.getClients().get(0));
        assertEquals(c2, dependency.getSuppliers().get(0));
    }

    /**
     * Basic test case for Generalization initialization.
     */
    @Test
    public void testGeneralization() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        Generalization generalization = create(Generalization.class);

        new ElementDomainBasedEdgeInitializer().initialize(generalization, c1, c2, null, null, null);

        assertEquals(c1, generalization.getSpecific());
        assertEquals(c2, generalization.getGeneral());

    }

    /**
     * Basic test case for {@link Substitution} initialization.
     */
    @Test
    public void testSubstitution() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        Substitution substitution = create(Substitution.class);

        new ElementDomainBasedEdgeInitializer().initialize(substitution, c1, c2, null, null, null);

        assertEquals(c1, substitution.getSubstitutingClassifier());
        assertEquals(c1, substitution.getClients().get(0));
        assertEquals(c2, substitution.getContract());
        assertEquals(c2, substitution.getSuppliers().get(0));

    }

    /**
     * Basic test case for {@link Manifestation} initialization.
     */
    @Test
    public void testManifestation() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        Manifestation manifestation = create(Manifestation.class);

        new ElementDomainBasedEdgeInitializer().initialize(manifestation, c1, c2, null, null, null);

        assertEquals(c1, manifestation.getClients().get(0));
        assertEquals(c2, manifestation.getUtilizedElement());
        assertEquals(c2, manifestation.getSuppliers().get(0));

    }

    /**
     * Basic test case for {@link PackageImport} initialization.
     */
    @Test
    public void testPackageImport() {
        Package source = create(Package.class);
        Package target = create(Package.class);
        PackageImport element = create(PackageImport.class);

        new ElementDomainBasedEdgeInitializer().initialize(element, source, target, null, null, null);

        assertEquals(source, element.getImportingNamespace());
        assertEquals(target, element.getImportedPackage());
    }

    /**
     * Basic test case for {@link PackageMerge} initialization.
     */
    @Test
    public void testPackageMerge() {
        Package source = create(Package.class);
        Package target = create(Package.class);
        PackageMerge element = create(PackageMerge.class);

        new ElementDomainBasedEdgeInitializer().initialize(element, source, target, null, null, null);

        assertEquals(source, element.getReceivingPackage());
        assertEquals(target, element.getMergedPackage());
    }

    /**
     * Basic test case for {@link Association} initialization.
     */
    @Test
    public void testAssociation() {
        Actor source = create(Actor.class);
        source.setName("Actor1");
        Actor target = create(Actor.class);
        target.setName("Actor2");
        Association element = create(Association.class);

        EMap<String, String> details = UML2Util.getEAnnotation(element, "org.eclipse.papyrus", true).getDetails();

        new ElementDomainBasedEdgeInitializer().initialize(element, source, target, null, null, null);

        // Check Nature
        assertTrue(details.containsKey(NATURE));
        assertEquals("UML_Nature", details.get(NATURE));

        List<Property> propList = element.getMemberEnds().stream().filter(Property.class::isInstance)
                .map(Property.class::cast).collect(Collectors.toList());

        assertEquals(target.getName().toLowerCase(), propList.get(0).getName());
        assertEquals(source.getName().toLowerCase(), propList.get(1).getName());

    }

    /**
     * Basic test case for {@link Association} initialization with an existing
     * EAnnotation key "nature". Expected : The key is remove and recreate.
     */
    @Test
    public void testAssociationWithExistingEAnnotationKey() {
        Actor source = create(Actor.class);
        source.setName("Actor1");
        Actor target = create(Actor.class);
        target.setName("Actor2");
        Association element = create(Association.class);

        EMap<String, String> details = UML2Util.getEAnnotation(element, "org.eclipse.papyrus", true).getDetails();

        details.put(NATURE, "already exist");
        new ElementDomainBasedEdgeInitializer().initialize(element, source, target, null, null, null);
        assertEquals("UML_Nature", details.get(NATURE));
    }

    /**
     * Basic test case for {@link Include} initialization.
     */
    @Test
    public void testInclude() {
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);

        new ElementDomainBasedEdgeInitializer().initialize(include, source, target, null, null, null);

        assertEquals(source, include.getIncludingCase());
        assertEquals(target, include.getAddition());

    }

    /**
     * Basic test case for {@link InformationFlow} initialization.
     */
    @Test
    public void testInformationFlow() {
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        InformationFlow informationFlow = create(InformationFlow.class);

        new ElementDomainBasedEdgeInitializer().initialize(informationFlow, c1, c2, null, null, null);

        assertEquals(c1, informationFlow.getInformationSources().get(0));
        assertEquals(c2, informationFlow.getInformationTargets().get(0));

    }

    /**
     * Basic test case for {@link Extend} initialization.
     */
    @Test
    public void testExtend() {
        Extend extend = create(Extend.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);

        new ElementDomainBasedEdgeInitializer().initialize(extend, source, target, null, null, null);

        assertEquals(source, extend.getExtension());
        assertEquals(target, extend.getExtendedCase());
        ExtensionPoint extensionPoint = extend.getExtensionLocations().get(0);
        assertNotNull(extensionPoint);

    }
}
