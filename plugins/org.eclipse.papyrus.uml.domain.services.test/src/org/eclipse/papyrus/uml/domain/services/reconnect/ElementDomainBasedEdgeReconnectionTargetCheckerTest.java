/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ElementDomainBasedEdgeReconnectionTargetCheckerTest extends AbstractUMLTest {

    private ElementDomainBasedEdgeReconnectionTargetChecker elementDomainBasedEdgeReconnectionTargetChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        elementDomainBasedEdgeReconnectionTargetChecker = new ElementDomainBasedEdgeReconnectionTargetChecker(
                e -> true);
    }

    @Test
    public void testCanReconnectAssociationTarget() {
        Model model = create(Model.class);
        UseCase targetUseCase = createIn(UseCase.class, model);
        UseCase newTargetUseCase = createIn(UseCase.class, model);
        Association association = createIn(Association.class, model);

        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association, targetUseCase,
                newTargetUseCase);
        assertTrue(status.isValid());

        Comment newTargetComment = createIn(Comment.class, model);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(association, targetUseCase,
                newTargetComment);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for PackageMerge.
     */
    @Test
    public void testCanReconnectPackageMergeTarget() {

        Package packSource = create(Package.class);
        Package packTarget = create(Package.class);
        Package packTarget2 = create(Package.class);

        PackageMerge merge = createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(merge, packTarget,
                packTarget2);
        assertTrue(status.isValid());

        // can't reconnect on non Package
        Actor errorTarget = create(Actor.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(merge, packTarget, errorTarget);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for PackageImport.
     */
    @Test
    public void testCanReconnectPackageImportTarget() {

        Package packSource = create(Package.class);
        Package packTarget = create(Package.class);
        Package packTarget2 = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(packImport, packTarget,
                packTarget2);
        assertTrue(status.isValid());

        // can reconnect on NameSpace
        Comment errorTarget = create(Comment.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(packImport, packTarget, errorTarget);
        assertFalse(status.isValid());
    }

    /**
     * Test reconnect target with null target => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNullTarget() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2,
                null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect target with no namedElement => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNoNamedElement() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Comment comment = create(Comment.class);

        // check creation is unauthorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2,
                comment);
        assertFalse(canCreateStatus.isValid());
        assertEquals("Dependency target can only be reconnected to a non null NamedElement.",
                canCreateStatus.getMessage());
    }

    /**
     * Test reconnect target with a named element => reconnection is authorized.
     */
    @Test
    public void testCanReconnectDependencyTargetWithNamedElement() {
        // create semantic elements
        Dependency dependency = create(Dependency.class);
        Class c1 = create(Class.class);
        Class c2 = create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Class c3 = create(Class.class);

        // check creation is authorized
        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(dependency, c2, c3);
        assertTrue(canCreateStatus.isValid());
    }

    @Test
    public void testCanReconnectTransitionTarget() {
        State state1 = create(State.class);
        Pseudostate source = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate target1 = createIn(Pseudostate.class, region);
        Pseudostate target2 = createIn(Pseudostate.class, region);

        Transition transition = createIn(Transition.class, region);
        transition.setSource(source);
        transition.setTarget(target1);

        CheckStatus canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(transition, target1,
                target2);
        assertTrue(canCreateStatus.isValid());

        canCreateStatus = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(transition, target1, region);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Default reconnection for Generalization.
     */
    @Test
    public void testCanReconnectGeneralizationTarget() {
        // create semantic elements
        Generalization generalization = create(Generalization.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newTarget = create(UseCase.class);

        // test reconnection on classifier
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target,
                newTarget);
        assertTrue(status.isValid());

        // test reconnection on its source
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target, source);
        assertFalse(status.isValid());

        // test reconnection on non classifier
        Package newTargetPackage = create(Package.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(generalization, target, newTargetPackage);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Substitution}.
     */
    @Test
    public void testCanReconnectSubstitutionTarget() {
        // create semantic elements
        Substitution substitution = create(Substitution.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        substitution.setSubstitutingClassifier(source);
        substitution.setContract(target);
        UseCase newTarget = create(UseCase.class);

        // test reconnection on classifier
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target,
                newTarget);
        assertTrue(status.isValid());

        // test reconnection on its source
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target, source);
        assertFalse(status.isValid());

        // test reconnection on non classifier
        Package newTargetPackage = create(Package.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(substitution, target, newTargetPackage);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Usage}.
     */
    @Test
    public void testCanReconnectUsageTarget() {
        // create semantic elements
        Usage usage = create(Usage.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        usage.getClients().add(source);
        usage.getSuppliers().add(target);
        UseCase newTarget = create(UseCase.class);

        // test reconnection on NamedElement
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(usage, target, newTarget);
        assertTrue(status.isValid());

        // test reconnection on non NamedElement
        Comment newTargetComment = create(Comment.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(usage, target, newTargetComment);
        assertFalse(status.isValid());
    }

    /**
     * Default reconnection for {@link Include}.
     */
    @Test
    public void testCanReconnectIncludeTarget() {
        // create semantic elements
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newTarget = create(UseCase.class);

        // check reconnection on UseCase
        CheckStatus status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target, newTarget);
        assertTrue(status.isValid());

        // cannot reconnect on the source
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target, source);
        assertFalse(status.isValid());

        // cannot reconnect on non {@link UseCase}
        Actor newErrorTarget = create(Actor.class);
        status = elementDomainBasedEdgeReconnectionTargetChecker.canReconnect(include, target, newErrorTarget);
        assertFalse(status.isValid());
    }
}
