/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

public class ElementDomainBasedEdgeReconnectTargetBehaviorProviderTest extends AbstractUMLTest {

    /**
     * Default reconnection for PackageMerge.
     */
    @Test
    public void testPackageMergeTargetReconnect() {

        Package packSource = create(Package.class);
        Package packTarget = create(Package.class);
        Package packTarget2 = create(Package.class);

        PackageMerge merge = createIn(PackageMerge.class, packSource);
        merge.setMergedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(merge,
                packTarget, packTarget2);
        assertTrue(status.isValid());
        assertEquals(packTarget2, merge.getMergedPackage());
    }

    /**
     * Default reconnection for PackageImport.
     */
    @Test
    public void testPackageImportTargetReconnect() {

        Package packSource = create(Package.class);
        Package packTarget = create(Package.class);
        Package packTarget2 = create(Package.class);

        PackageImport packImport = createIn(PackageImport.class, packSource);
        packImport.setImportedPackage(packTarget);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(packImport,
                packTarget, packTarget2);
        assertTrue(status.isValid());
        assertEquals(packTarget2, packImport.getImportedPackage());
    }

    /**
     * Test reconnect target with null dependency or null old target or null new
     * target.
     */
    @Test
    public void testDependencyTargetReconnectWithNullArguments() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(null, c1, c2);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(dependency, c1, null);

        assertFalse(status.isValid());

        status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(dependency, null, c1);

        assertFalse(status.isValid());

    }

    /**
     * Test reconnect target of dependency on an other target in the same package.
     * Expected result : target changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencyTargetReconnectInSamePackage() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        org.eclipse.uml2.uml.Class c3 = createIn(org.eclipse.uml2.uml.Class.class, p1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(dependency, c2,
                c3);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getSuppliers().size());
        assertEquals(c3, dependency.getSuppliers().get(0));
    }

    /**
     * Test reconnect target of dependency on an other target in different package.
     * Expected result : target changed but the dependency remains in its owner
     * package
     */
    @Test
    public void testDependencyTargetReconnectInDifferentPackage() {
        org.eclipse.uml2.uml.Package p1 = create(org.eclipse.uml2.uml.Package.class);
        Dependency dependency = createIn(Dependency.class, p1);
        org.eclipse.uml2.uml.Class c1 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        org.eclipse.uml2.uml.Class c2 = createIn(org.eclipse.uml2.uml.Class.class, p1);
        p1.getPackagedElements().add(c2);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        org.eclipse.uml2.uml.Package p2 = create(org.eclipse.uml2.uml.Package.class);
        org.eclipse.uml2.uml.Class c3 = createIn(org.eclipse.uml2.uml.Class.class, p2);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(dependency, c2,
                c3);

        assertTrue(status.isValid());
        assertEquals(p1, dependency.getOwner());
        assertEquals(1, dependency.getSuppliers().size());
        assertEquals(c3, dependency.getSuppliers().get(0));
    }

    @Test
    public void testTransitionTargetReconnect() {
        State state1 = create(State.class);
        Pseudostate source = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate target1 = createIn(Pseudostate.class, region);
        Pseudostate target2 = createIn(Pseudostate.class, region);

        Transition transition = createIn(Transition.class, region);
        transition.setSource(source);
        transition.setTarget(target1);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(transition,
                target1, target2);

        assertTrue(status.isValid());
        assertEquals(region, transition.getOwner());
        assertEquals(source, transition.getSource());
        assertEquals(target2, transition.getTarget());
    }

    /**
     * Test reconnect target of association on an other target in different actor.
     * Expected result : target changed
     */
    @Test
    public void testAssociationTargetReconnect() {
        Actor source = create(Actor.class);
        Property sourceProp = create(Property.class);
        sourceProp.setType((Classifier) source);

        Actor oldTarget = create(Actor.class);
        Property targetProp = create(Property.class);
        targetProp.setType((Classifier) oldTarget);

        Actor newTarget = create(Actor.class);

        Association association = create(Association.class);
        association.getMemberEnds().add(targetProp);
        association.getMemberEnds().add(sourceProp);
        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(association,
                oldTarget, newTarget);

        assertTrue(status.isValid());
        assertEquals(2, association.getMemberEnds().size());
        assertEquals((Classifier) source, association.getMemberEnds().get(1).getType());
        assertEquals((Classifier) newTarget, association.getMemberEnds().get(0).getType());

    }

    @Test
    public void testGeneralizationTargetReconnect() {
        UseCase target = create(UseCase.class);
        UseCase target2 = create(UseCase.class);

        Generalization generalization = create(Generalization.class);
        generalization.setGeneral(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(generalization,
                target, target2);

        assertTrue(status.isValid());
        assertEquals(target2, generalization.getGeneral());
    }

    /**
     * Default target reconnection for {@link Include}.
     */
    @Test
    public void testIncludeTargetReconnect() {

        UseCase target = create(UseCase.class);
        UseCase target2 = create(UseCase.class);

        Include include = create(Include.class);
        include.setAddition(target);

        CheckStatus status = new ElementDomainBasedEdgeReconnectTargetBehaviorProvider().reconnectTarget(include,
                target, target2);

        assertTrue(status.isValid());
        assertEquals(target2, include.getAddition());
    }
}
