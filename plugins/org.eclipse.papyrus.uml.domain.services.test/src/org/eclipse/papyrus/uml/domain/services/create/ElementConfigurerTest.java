/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

public class ElementConfigurerTest extends AbstractUMLTest {

    private UMLFactory fact = UMLFactory.eINSTANCE;

    @Test
    public void testNullInput() {
        assertNull(new ElementConfigurer().configure(null, null));
    }

    @Test
    public void testNamedElement() {

        var pack = create(Package.class);
        var c1 = create(Class.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(c1, pack);

        assertEquals("Class1", c1.getName()); //$NON-NLS-1$
        pack.getPackagedElements().add(c1);

        var c2 = this.fact.createClass();
        elementInitializer.configure(c2, pack);
        // Create a second one
        assertEquals("Class2", c2.getName()); //$NON-NLS-1$

        var c17 = this.fact.createClass();
        c17.setName("Class17"); //$NON-NLS-1$
        pack.getPackagedElements().add(c17);

        var c18 = this.fact.createClass();
        elementInitializer.configure(c18, pack);
        c17.setName("Class18"); //$NON-NLS-1$
    }

    @Test
    public void testPortCreation() {

        var c1 = create(Class.class);
        var port = create(Port.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(port, c1);

        assertEquals("Port1", port.getName()); //$NON-NLS-1$
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port.getAggregation());
        c1.getOwnedPorts().add(port);

        var port2 = this.fact.createPort();
        elementInitializer.configure(port2, c1);

        // Create a second one
        assertEquals("Port2", port2.getName()); //$NON-NLS-1$
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port2.getAggregation());

    }

    @Test
    public void testConstraintCreation() {

        var m1 = create(Model.class);
        var constraint = create(Constraint.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(constraint, m1);

        assertEquals("Constraint1", constraint.getName()); //$NON-NLS-1$
        assertEquals("constraintSpec", constraint.getSpecification().getName()); //$NON-NLS-1$
        m1.getOwnedRules().add(constraint);

        var constraint2 = this.fact.createConstraint();
        elementInitializer.configure(constraint2, m1);

        // Create a second one
        assertEquals("Constraint2", constraint2.getName()); //$NON-NLS-1$
        assertEquals("constraintSpec", constraint2.getSpecification().getName()); //$NON-NLS-1$
    }

    /**
     * Test the configuration of a usage => It should not have a default name.
     */
    @Test
    public void testUsageConfiguration() {
        var pack = create(Package.class);
        var usage = create(Usage.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(usage, pack);

        assertNull(usage.getName());
    }

    /**
     * Test the configuration of a UseCase => If its owner is a Classifier, useCase
     * should be added to useCase feature (subject feature of useCase should be
     * automatically updated).
     */
    @Test
    public void testUseCaseConfiguration() {
        Activity activity = create(Activity.class);
        UseCase useCase = create(UseCase.class);
        activity.getOwnedUseCases().add(useCase);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(useCase, activity);

        assertTrue(activity.getUseCases().contains(useCase));
        assertTrue(useCase.getSubjects().contains(activity));

        UseCase useCase2 = create(UseCase.class);
        Model model = create(Model.class);
        model.getPackagedElements().add(useCase2);
        elementInitializer.configure(useCase2, model);
        assertTrue(useCase2.getSubjects().isEmpty());
    }

    /**
     * Test the configuration of a Property => If its owner is a Collaboration, the
     * property should be added to collaborationRoles feature.
     */
    @Test
    public void testPropertyConfiguration() {
        Collaboration collaboration = create(Collaboration.class);
        Property property = create(Property.class);
        collaboration.getOwnedAttributes().add(property);
        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(property, collaboration);

        assertTrue(collaboration.getOwnedAttributes().contains(property));
        assertTrue(collaboration.getCollaborationRoles().contains(property));

        Property property2 = create(Property.class);
        Class class1 = create(Class.class);
        class1.getOwnedAttributes().add(property2);
        elementInitializer.configure(property2, class1);
        assertTrue(class1.getOwnedAttributes().contains(property2));
    }

}
