/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.edges;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Abstraction;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Realization;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeContainerProvider}.
 * 
 * @author Arthur Daussy
 *
 */
public class ElementDomainBasedEdgeContainerProviderTest extends AbstractUMLTest {

    /**
     * Test basic uses case for dependency creation (all element are editable) when
     * the source and the target have a common package ancestor.
     */
    @Test
    public void testDependencyDirectCommonAncerstor() {

        Package pack = create(Package.class);
        Class c1 = createIn(Class.class, pack);
        Class c2 = createIn(Class.class, pack);

        Dependency dependency = create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                IEditableChecker.TRUE);

        // Visual element are not used for this use case
        EObject container = containerProvider.getContainer(c1, c2, dependency, null, null, null);

        assertEquals(pack, container);
    }

    /**
     * Test basic uses case for dependency creation (all element are editable) when
     * the source and the target have a common package ancestor.
     */
    @Test
    public void testDependencyNoDirectCommonAncestor() {

        Package pack = create(Package.class);
        Class c1 = createIn(Class.class, pack);
        Class c2 = createIn(Class.class, pack);
        Class c1c1 = createIn(Class.class, c1);
        Class c2c2 = createIn(Class.class, c2);

        Dependency dependency = create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // Visual element are not used for this use case
        EObject container = containerProvider.getContainer(c1c1, c2c2, dependency, null, null, null);

        assertEquals(pack, container);
    }

    /**
     * Test that uneditable element can't be used as container.
     *
     */
    @Test
    public void testDependencyUneditableNotDirectCommonAncestor() {
        Package pack = create(Package.class);
        Class c1 = createIn(Class.class, pack);
        Class c2 = createIn(Class.class, pack);
        Class c1c1 = createIn(Class.class, c1);
        Class c2c2 = createIn(Class.class, c2);

        Dependency dependency = create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> e != pack);

        // Visual element are not used for this use case
        EObject container = containerProvider.getContainer(c1c1, c2c2, dependency, null, null, null);

        assertNull(container);
    }

    /**
     * Checks the different fallback cases for container computation.
     */
    @Test
    public void testDependencyFallbackCases() {
        Package pack1 = create(Package.class);
        Class c1 = createIn(Class.class, pack1);
        Package pack2 = create(Package.class);
        Class c2 = createIn(Class.class, pack2);

        // No common ancestor => source package

        Dependency dependency = create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(c1, c2, dependency, null, null, null);

        assertEquals(pack1, container);

        // If C1 is uneditable => C2

        containerProvider = new ElementDomainBasedEdgeContainerProvider(e -> pack1 != e);

        container = containerProvider.getContainer(c1, c2, dependency, null, null, null);

        assertEquals(pack2, container);

        // If bith C1 and C2 are uneditable then null

        containerProvider = new ElementDomainBasedEdgeContainerProvider(e -> pack1 != e && e != pack2);

        container = containerProvider.getContainer(c1, c2, dependency, null, null, null);

        assertNull(container);

        // Test that if the source is a CollaborationUse, the container is the
        // CollaborationUse

        CollaborationUse collaborationUse = create(CollaborationUse.class);
        c2.getCollaborationUses().add(collaborationUse);
        Dependency dependency2 = create(Dependency.class);
        EObject container2 = containerProvider.getContainer(collaborationUse, c1, dependency2, null, null, null);
        assertEquals(collaborationUse, container2);
    }

    /**
     * Test the basic case for {@link Generalization}.
     */
    @Test
    public void testGeneralization() {
        Class source = create(Class.class);
        Class target = create(Class.class);
        Generalization generalization = create(Generalization.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, generalization, null, null, null);

        assertEquals(source, container);
    }

    /**
     * Test the basic case for {@link Realization}.
     */
    @Test
    public void testRealization() {
        Model model = create(Model.class);
        Class source = create(Class.class);
        Class target = create(Class.class);
        CollaborationUse collaborationUse = create(CollaborationUse.class);
        model.getPackagedElements().add(source);
        model.getPackagedElements().add(target);
        source.getCollaborationUses().add(collaborationUse);
        Realization realization = create(Realization.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, realization, null, null, null);

        assertEquals(model, container);

        // Test that if the source is a CollaborationUse, the container is the
        // CollaborationUse
        Realization realization2 = create(Realization.class);
        EObject container2 = containerProvider.getContainer(collaborationUse, target, realization2, null, null, null);
        assertEquals(collaborationUse, container2);
    }

    /**
     * The container of DirectedRelationship is the source if its can semantically
     * contains the edge.
     */
    @Test
    public void testAbstractionWithNonCapableContainingSource() {

        Package pack = create(Package.class);
        Class source = createIn(Class.class, pack);
        Class target = createIn(Class.class, pack);

        Abstraction abstraction = create(Abstraction.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // In this case the most common ancestor
        EObject container = containerProvider.getContainer(source, target, abstraction, null, null, null);
        assertEquals(pack, container);

    }

    /**
     * The container of DirectedRelationship is the most common package ancestor to
     * the source and the target is the source if its can <b>not</b> semantically
     * contains the edge.
     */
    @Test
    public void testAbstractionWithContainingCapableSource() {
        Package source = create(Package.class);
        Class target = create(Class.class);

        Abstraction abstraction = create(Abstraction.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // In this case the most common ancestor
        EObject container = containerProvider.getContainer(source, target, abstraction, null, null, null);
        assertEquals(source, container);

    }

    /**
     * The container of DirectedRelationship is the source if its can semantically
     * contains the edge.
     */
    @Test
    public void testDependencyWithNonCapableContainingSource() {

        Package pack = create(Package.class);
        Class source = createIn(Class.class, pack);
        Class target = createIn(Class.class, pack);

        Dependency dependency = create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // In this case the most common ancestor
        EObject container = containerProvider.getContainer(source, target, dependency, null, null, null);
        assertEquals(pack, container);

    }

    /**
     * The container of DirectedRelationship is the most common package ancestor to
     * the source and the target is the source if its can <b>not</b> semantically
     * contains the edge.
     */
    @Test
    public void testDependencyWithContainingCapableSource() {
        Package source = create(Package.class);
        Class target = create(Class.class);

        Dependency dependency = create(Dependency.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        // In this case the most common ancestor
        EObject container = containerProvider.getContainer(source, target, dependency, null, null, null);
        assertEquals(source, container);

    }

    /**
     * Test the basic case for {@link Substitution}.
     */
    @Test
    public void testSubstitution() {
        Class source = create(Class.class);
        Class target = create(Class.class);
        Substitution substitution = create(Substitution.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, substitution, null, null, null);

        assertEquals(source, container);
    }

    @Test
    public void testTransition() {
        StateMachine stateMachine = create(StateMachine.class);
        Region regionSM = createIn(Region.class, stateMachine);
        State state1 = createIn(State.class, regionSM);
        Pseudostate entryPoint = createIn(Pseudostate.class, state1);
        Region region = createIn(Region.class, state1);
        Pseudostate fork = createIn(Pseudostate.class, region);

        Transition transition = create(Transition.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(entryPoint, fork, transition, null, null, null);
        assertEquals(regionSM, container);

        container = containerProvider.getContainer(fork, entryPoint, transition, null, null, null);
        assertEquals(region, container);
    }

    /**
     * Test association between two {@link Class} of {@link Model} => container
     * should be the {@link Model}.
     */
    @Test
    public void testAssociationInModel() {
        Model model = create(Model.class);
        Class source = create(Class.class);
        Class target = create(Class.class);
        Association association = create(Association.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode rootNode = visualTree.addChildren(model);
        VisualNode visualClass1 = rootNode.addChildren(source);
        VisualNode visualClass2 = rootNode.addChildren(target);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, association, new MockedViewQuerier(rootNode),
                visualClass1, visualClass2);

        assertEquals(model, container);
    }

    /**
     * Test association between two {@link Class} of {@link Package} in
     * {@link Model} => container should be the {@link Model}.
     */
    @Test
    public void testAssociationInSubPackage() {
        Model model = create(Model.class);
        Package pack = create(Package.class);
        Class source = create(Class.class);
        Class target = create(Class.class);
        Association association = create(Association.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode rootNode = visualTree.addChildren(model);
        VisualNode packNode = rootNode.addChildren(pack);
        VisualNode visualClass1 = packNode.addChildren(source);
        VisualNode visualClass2 = packNode.addChildren(target);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, association, new MockedViewQuerier(rootNode),
                visualClass1, visualClass2);

        assertEquals(model, container);
    }

    /**
     * Test the basic case for {@link Include}.
     */
    @Test
    public void testInclude() {
        Include include = create(Include.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, include, null, null, null);

        assertEquals(source, container);
    }

    /**
     * Test the basic case for {@link Extend}.
     */
    @Test
    public void testExtend() {
        Extend extend = create(Extend.class);
        UseCase source = create(UseCase.class);
        UseCase target = create(UseCase.class);

        ElementDomainBasedEdgeContainerProvider containerProvider = new ElementDomainBasedEdgeContainerProvider(
                e -> true);

        EObject container = containerProvider.getContainer(source, target, extend, null, null, null);

        assertEquals(source, container);
    }

}
